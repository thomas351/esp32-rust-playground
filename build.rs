fn main() {
    embuild::espidf::sysenv::output();
    println!("cargo::rustc-check-cfg=cfg(esp_idf_app_compile_time_date)");
    println!("cargo::rustc-check-cfg=cfg(esp_idf_app_reproducible_build)");
    println!("cargo::rustc-check-cfg=cfg(esp_idf_version_major, values(\"4\"))");
    println!("cargo::rustc-check-cfg=cfg(esp_idf_version, values(\"5.0\"))");
    println!("cargo::rustc-check-cfg=cfg(esp_idf_version, values(\"5.1\"))");
    println!("cargo::rustc-check-cfg=cfg(esp_idf_version, values(\"5.2\"))");
    println!("cargo::rustc-check-cfg=cfg(esp_idf_version, values(\"5.3\"))");
    println!("cargo::rustc-check-cfg=cfg(esp_idf_version_full, values(\"5.2.0\"))");
    println!("cargo::rustc-check-cfg=cfg(esp_idf_version_full, values(\"5.2.1\"))");
    println!("cargo::rustc-check-cfg=cfg(esp_idf_version_full, values(\"5.2.2\"))");
    println!("cargo::rustc-check-cfg=cfg(esp_idf_version_full, values(\"5.2.3\"))");
    println!("cargo::rustc-check-cfg=cfg(esp_idf_version_full, values(\"5.3.0\"))");
    println!("cargo::rustc-check-cfg=cfg(esp_idf_version_full, values(\"5.3.1\"))");
    println!("cargo::rustc-check-cfg=cfg(esp_idf_version_full, values(\"5.3.2\"))");
}
