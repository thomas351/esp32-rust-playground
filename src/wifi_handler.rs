use std::{str::FromStr, time::Duration};

use crossbeam_channel::Sender;
use embedded_svc::wifi::{self, AccessPointConfiguration, ClientConfiguration};
use esp_idf_svc::{
    hal::modem::Modem,
    nvs::EspDefaultNvsPartition,
    sys::{esp_timer_get_time, esp_wifi_sta_get_ap_info, wifi_ap_record_t},
    wifi::EspWifi,
};
use hex::ToHex;
use log::{error, info, log, trace, warn};

use crate::{
    app_state::APP_STATE,
    config::{CONFIG, DERIVED_CONFIG},
    human_readable_duration::format_duration,
};

pub fn wifi_info(tx: &Sender<String>, force_log: bool, wifi: &mut EspWifi<'_>) {
    let mut info = wifi_ap_record_t::default();
    unsafe {
        esp_wifi_sta_get_ap_info(&mut info);
    }
    let bssid: heapless::String<12> = info.bssid.encode_hex();
    tx.try_send(format!(
        ",mac={},bssid={bssid} esp32.rssi={}",
        APP_STATE.mac(),
        info.rssi
    ))
    .ok();

    log!(
        if force_log {
            log::Level::Info
        } else {
            DERIVED_CONFIG.log_wifi_info
        },
        "wifi ap info: {:?}",
        info
    );
    log!(
        if force_log {
            log::Level::Info
        } else {
            DERIVED_CONFIG.log_wifi_info
        },
        "Wifi connection info: {:?}",
        wifi.sta_netif().get_ip_info(),
    );
}

fn wifi_settled(force_log: bool) -> bool {
    let wifi_time = match APP_STATE.wifi_time.try_read() {
        Some(guard) => guard,
        None => {
            warn!("check_wifi couldn't acquire wifi_time write lock!");
            return false;
        }
    };

    if let Some(time) = APP_STATE.ota_time.read().as_ref() {
        let passed = unsafe { esp_timer_get_time() } - time;
        let log_line = format!(
            "last ota attempt started {} ago. ",
            format_duration(Duration::from_micros(passed as _))
        );
        if passed < 5 * 60 * 1000 * 1000 {
            info!("{log_line}don't mess wifi wifi for at least 5 minutes to give it a chance to finish.");
            return false;
        }
    }

    if let Some(time) = wifi_time.as_ref() {
        let passed = unsafe { esp_timer_get_time() } - time;
        let log_line = format!(
            "last wifi connection attempt started {} ago. ",
            format_duration(Duration::from_micros(passed as _))
        );
        if passed < 60 * 1000 * 1000 {
            info!("{log_line}let it settle a minute before checking again");
        } else {
            log!(
                if force_log {
                    log::Level::Info
                } else {
                    DERIVED_CONFIG.log_wifi_info
                },
                "{log_line}checking if it's connected now"
            );
            return true;
        }
    } else {
        error!("wifi setup failed to set wifi_time, check implementation!");
    };
    false
}

pub fn check_wifi(tx: &Sender<String>, force_log: bool) {
    log!(
        if force_log {
            DERIVED_CONFIG.log_max_level
        } else {
            DERIVED_CONFIG.log_wifi_info
        },
        "check_wifi.started"
    );

    if !wifi_settled(force_log) {
        return;
    }
    let mut wifi_guard = match APP_STATE.wifi.try_lock() {
        Err(error) => {
            warn!("check_wifi couldn't acquire wifi lock, error={error}");
            return;
        }
        Ok(guard) => guard,
    };
    let wifi = match wifi_guard.get_mut() {
        None => {
            error!("Wifi Setup failed or didn't run! APP_STATE.wifi is None!");
            return;
        }
        Some(wifi) => wifi,
    };
    wifi_info(tx, force_log, wifi);

    match wifi.is_connected() {
        Ok(true) => {
            trace!("check_wifi: it's connected :)");
        }
        Ok(false) => {
            warn!("check_wifi: not connected! trying to reconnect now!");
            APP_STATE.update_wifi_time();
            match wifi.stop() {
                Ok(_) => info!("wifi.stop succeeded"),
                Err(error) => error!("wifi.stop failed: {error}"),
            }
            match wifi.start() {
                Ok(_) => info!("wifi.start succeeded"),
                Err(error) => error!("wifi.start failed: {error}"),
            }
            match wifi.connect() {
                Ok(_) => info!("wifi.connect succeeded"),
                Err(error) => error!("wifi.connect failed: {error}"),
            }
        }
        Err(error) => {
            error!("check_wifi() -> wifi.is_connected() failed: {error}")
        }
    }
}

pub fn wifi_setup(modem: Modem) -> anyhow::Result<()> {
    println!("running wifi setup now");
    let mut wifi = EspWifi::new(
        modem,
        APP_STATE.sysloop.clone(),
        Some(EspDefaultNvsPartition::take()?),
    )?;

    println!("Wifi created, processing mac");

    let mac: heapless::String<12> = wifi.ap_netif().get_mac()?.encode_hex();
    let mut ap_name: heapless::String<32> = heapless::String::from_str("ESP32_").unwrap();
    ap_name.push_str(mac.as_str()).ok();
    APP_STATE.set_mac(mac).ok();

    println!("mac processed, now setting configuration");

    wifi.set_configuration(&wifi::Configuration::Mixed(
        ClientConfiguration {
            ssid: heapless::String::from_str(CONFIG.wifi_ssid).unwrap(),
            password: heapless::String::from_str(CONFIG.wifi_psk).unwrap(),
            channel: None,
            ..Default::default()
        },
        AccessPointConfiguration {
            ssid: ap_name,
            password: heapless::String::from_str(CONFIG.wifi_ap_pwd).unwrap(),
            channel: 1,
            auth_method: wifi::AuthMethod::WPA2Personal,
            ..Default::default()
        },
    ))?;

    println!("Starting wifi...");
    wifi.start().ok();

    println!("Connecting wifi...");
    // this might fail, but still be okay at least for the fallback AP. Prevent Error propagation to enable OTA via fallback AP.
    wifi.connect().ok();

    println!("store wifi driver into app state");
    match APP_STATE.wifi.try_lock() {
        Ok(cell) => {
            cell.set(wifi).ok();
        }
        Err(error) => error!("APP_STATE.wifi.try_lock() failed: {error}"),
    }

    APP_STATE.update_wifi_time();

    println!("wifi setup done!");
    Ok(())
}
