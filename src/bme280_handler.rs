use bme280_rs::{Bme280, Oversampling, SensorMode};
use core::fmt;
use crossbeam_channel::Sender;
use esp_idf_svc::hal::delay;
use esp_idf_svc::sys::{esp_restart, esp_timer_get_time};
use log::{debug, error, info, log, trace, warn};
use std::{borrow::Cow, time::Duration};
use strum::EnumCount;
#[cfg(feature = "display")]
use strum::IntoEnumIterator;
use strum_macros::{EnumCount, EnumIter, EnumString};

use crate::{
    app_state::APP_STATE,
    config::{CONFIG, DERIVED_CONFIG},
    human_readable_duration::format_duration,
};

#[derive(Clone, Copy, Debug)]
pub struct Measurement {
    pub temperature_c: f32,
    pub pressure_pa: f32,
    pub rel_humidity_pct: f32,
    pub sensor_reads: u8,
    pub avg: bool,
}

#[derive(Debug, EnumIter, PartialEq, EnumCount, EnumString, Clone)]
pub enum MeasurementVariable {
    Temperature,
    HumidityRel,
    HumidityAbs,
    PressureBar,
    DewPoint,
    DewPointB,
}

#[cfg(feature = "display")]
impl MeasurementVariable {
    pub fn progress_variable(variable: MeasurementVariable, progress: i32) -> MeasurementVariable {
        let pos = MeasurementVariable::iter()
            .position(|x| x == variable)
            .unwrap();
        let step1 = (pos as i32 + progress) % MeasurementVariable::COUNT as i32;
        let next_index = if step1 < 0 {
            (step1 + MeasurementVariable::COUNT as i32) as usize
        } else {
            step1 as usize
        };
        match MeasurementVariable::iter().nth(next_index as usize) {
            Some(x) => {
                debug!("current MeasurementVariable = {variable:?} = #{pos} -> +{progress} = {next_index} = {x:?}");
                x
            }
            None => {
                error!("current MeasurementVariable = {variable:?} = #{pos} -> +{progress} = {next_index} gave None?! switch failed!");
                variable
            }
        }
    }
    pub fn get_unit(variable: &MeasurementVariable) -> &'static str {
        match variable {
            MeasurementVariable::Temperature => "°C",
            MeasurementVariable::HumidityRel => "%",
            MeasurementVariable::HumidityAbs => "g/m³",
            MeasurementVariable::PressureBar => "Bar",
            MeasurementVariable::DewPoint => "TP°C",
            MeasurementVariable::DewPointB => "DP°C",
        }
    }
}

// https://en.wikipedia.org/wiki/Kelvin
const KELVIN_CELSIUS_DIFF: f32 = 273.16;
// https://en.wikipedia.org/wiki/Avogadro_constant
const AVOGADRO_REC_MOL_10POW_23: f64 = 6.02214076;
// https://en.wikipedia.org/wiki/Boltzmann_constant
const BOLTZMANN_10_POW_M23: f64 = 1.380649;
// https://en.wikipedia.org/wiki/Gas_constant // = 8.314_462_618_153_24
const UNIVERSAL_GAS_CONSTANT_J_PER_MOL_K: f64 = AVOGADRO_REC_MOL_10POW_23 * BOLTZMANN_10_POW_M23;
// https://en.wikipedia.org/wiki/Molar_mass // std-deviation = 0.00007
// const HYDROGEN_GRAM_PER_MOL: f64 = 1.00797;
// https://www.thoughtco.com/how-much-water-is-a-mole-608527
// const HYDROGEN_GRAM_PER_MOL: f64 = 1.0079;
// const OXYGEN_GRAM_PER_MOL: f64 = 15.9994;
// https://en.wikipedia.org/wiki/Water // std-deviation = 0.00033
const WATER_WEIGHT_G_PER_MOL: f64 = 18.01528;
// to match above water weight specified at Wikipedia together with the hydrogen weight also from Wikipedia using the H₂O formula, Oxygen's weight needs to be:
// const OXYGEN_GRAM_PER_MOL: f64 = 15.99934;
// const WATER_WEIGHT_G_PER_MOL: f64 = 2 * HYDROGEN_GRAM_PER_MOL + OXYGEN_GRAM_PER_MOL;
// const WATER_WEIGHT_G_PER_MOL: f64 = 18.01528;
const GAS_CONSTANT_WATER_J_PER_G_K: f64 =
    UNIVERSAL_GAS_CONSTANT_J_PER_MOL_K / WATER_WEIGHT_G_PER_MOL;
const SATURATED_WATER_VAPOUR_0C_PA: f32 = 611.3;
// const

const fn arden_buck_constants(negative: bool) -> (f32, f32, f32, f32) {
    if negative {
        (611.15, 23.036, 333.7, 279.82)
    } else {
        (611.21, 18.678, 234.5, 257.14)
    }
}

/*
https://physics.stackexchange.com/questions/777960/how-does-athmospheric-pressure-influence-dewpoint
https://physics.stackexchange.com/questions/371175/what-law-or-formula-discusses-the-relationship-between-pressure-and-dew-point

Disclaimer: I'm not a physicist or chemist, just a software developer who struggled with all those water vapor related concepts for a few weeks himself.

## Dew-point vs. boiling-point
I'm not sure if this holds true for all substances, but water does not only vaporize when it reaches it's boiling point. Instead, a few molecules will always leave the surface of liquid or even frozen water. Depending on the partial water vapor pressure of the surrounding gas mixture, those might be balanced by the water molecules that condense / sublime back into liquid or solid phase. The temperature at which those two processes balance is called dew-point.

## The dew-point depends on
 * the temperature (of the water in all it's states)
 * the partial water vapor pressure
 * the surface geometry of the liquid / solid water
 * presence and abundance of particles suspended in the air mixture that promote condensation on their surface
 * presence and concentration of salt or other impurities on the surface of the liquid / solid water

As user @pentane has stated in his answer, the boiling-point does depend on the total air pressure, the dew-point in contrast depends on the partial water vapor pressure instead.
Unlike the boiling-point,

# **The dew-point does NOT depend on the total air pressure in the system!**

The Pressure Dew Point (PDP) formulas that user @tony-dinitto talks about in his answer let you calculate how the dew-point changes when you change the pressure in the system while holding the percentage of water vapor in the air mixture constant.

If you're like me dealing with a system, where the sensor measures temperature, relative humidity and total air pressure "at the same time" -
and you don't need to predict what would happen if you encapsulated this exact air mixture and compressed or expanded it, then Pressure Dew Point (PDP) formulas are of no use to you.

[According to Wikipedia](https://en.wikipedia.org/wiki/Vapour_pressure_of_water#Accuracy_of_different_formulations), the [Arden Buck Equation](https://en.wikipedia.org/wiki/Arden_Buck_equation) is the most accurate formula to calculate the saturated water vapor pressure for a given temperature. Using this fact I came up with the following way to

## calculate the dew-point:
1. Calculate saturated water vapor pressure for current temperature using the Arden Buck Equation.
2. Calculate the current water vapor pressure by multiplying the saturated water vapor by the relative humidity as a factor (divide by 100 if given as percent)
3. Use the inverse of the Arden Buck Equation to calculate which temperature would have the current water vapor pressure as saturated water vapor pressure.

Since for me it was quite difficult to find the inverse of the Arden Buck Equation, I'll post what I came up with to make it easier for readers who try to follow my steps:
```rust
let (a, b, c, d) = (611.21, 18.678, 234.5, 257.14); // empirical constants of the Arden Buck Equation for temperatures > 0°C
let g = (actual_water_vapor_pressure_pa / a).ln();
-0.5 * c * (((b - g).powi(2) - (4.0 * d * g) / c).sqrt() + g - b)
```

################################################################################

https://physics.stackexchange.com/questions/398413/does-the-saturated-vapor-pressure-depend-on-the-external-pressure-or-just-only-t
User @Chemomechanics contradicts above @2022-01-09 18:42:
I derive the pressure dependence here; it is typically small but not zero. Link target:
https://physics.stackexchange.com/questions/629640/why-atmospheric-pressure-does-not-influence-vapor-pressure-of-water-but-only-tem/629689#629689

################################################################################

https://physics.stackexchange.com/questions/141692/saturated-water-vapour-pressure
Let's assume the air you're compressing has around 50% relative humidity at 1bar, not an unlikely value for average weather conditions. That means the actual water vapor pressure of this air mixture is 50% of the saturated water vapor pressure for this temperature. Compressing it to twice the current pressure (~2bar / ~2atm) will not only double the dry air components pressure, but also the actual water vapor pressure. So you'll already be at 100% of the saturated water vapor pressure for the current temperature on 2bar. By increasing pressure to 200bar, you're adding another factor of 100 on top. Therefore, once the your bottle cools down to ambient temperatures, around 99% of the water vapor pumped into it at 50% relative humidity will condense to get the actual water vapor pressure inside the bottle down to 100% of the saturation water vapor pressure, which means you'll still have 100% relative humidity.
You said to assume that around 5cm² of all that condensed water will have stayed in your 215cm² bottle, so ~2.3% of the volume ...
(no point in finishing my reply, the question is locked and without enough fake internet points we can't post an answer)

*/

impl Measurement {
    pub fn dewpoint(&self) -> f32 {
        let a = (17.67 * self.temperature_c) / (243.5 + self.temperature_c);
        let b = (self.rel_humidity_pct / 100.0).ln();
        (243.5 * (b + a)) / (17.67 - b - a)
    }

    // don't know how we came to this formula, fact is it doesn't work...
    // pub fn dewpoint_from_vapor_pressure_c(&self) -> f32 {
    //     let p = self.actual_water_vapor_pressure_pa() / 100.0;
    //     let z = SATURATED_WATER_VAPOUR_0C_PA / 100.0;
    //     let a = (z * p).ln();
    //     (237.3 * a) / (17.27 - a)
    // }

    /*
    To isolate the temperature `t` from the equation `p = a * ((b - (t / c)) * (t / (d + t))).exp()`, we can follow these steps:

    Step 1: Take the natural logarithm (ln) of both sides of the equation:
    ln(p) = ln(a * ((b - (t / c)) * (t / (d + t))).exp())

    Step 2: Apply the logarithmic property of ln(a * b) = ln(a) + ln(b):
    ln(p) = ln(a) + ln(((b - (t / c)) * (t / (d + t))).exp())

    Step 3: Apply the logarithmic property of ln(e^x) = x:
    ln(p) = ln(a) + ((b - (t / c)) * (t / (d + t)))

    Step 4: Simplify the equation:
    ln(p) = ln(a) + (b - (t / c)) * (t / (d + t))

    Step 5: Distribute and rearrange the equation:
    ln(p) = ln(a) + (bt / (d + t)) - (t^2 / (c(d + t)))

    Step 6: Move the terms involving `t` to one side of the equation:
    (bt / (d + t)) - (t^2 / (c(d + t))) = ln(p) - ln(a)

    Step 7: Find a common denominator:
    ((bt)(c(d + t)) - (t^2)(d + t)) / (c(d + t)) = ln(p) - ln(a)

    Step 8: Simplify the equation:
    (bct(d + t) - (t^2)(d + t)) / (c(d + t)) = ln(p) - ln(a)

    Step 9: Expand and rearrange the equation:
    t^2(d + t) - bct(d + t) = (ln(a) - ln(p))(c(d + t))

    Step 10: Combine like terms:
    t^2(d + t) - bct(d + t) - (ln(a) - ln(p))(c(d + t)) = 0

    Step 11: Factor out the common factor (d + t):
    (d + t)(t - bc - (ln(a) - ln(p))c) = 0

    Step 12: Solve for `t`:
    (d + t) = 0 or (t - bc - (ln(a) - ln(p))c) = 0

    If (d + t) = 0, then we have d + t = 0, which implies t = -d.
    If (t - bc - (ln(a) - ln(p))c) = 0, then we solve for `t`:
    t = bc + (ln(a) - ln(p))c

    Thus, we have two solutions for `t`: t = -d and t = bc + (ln(a) - ln(p))c.
    However, since we assume the temperature `t` is greater than 0°C, the viable solution is:

    t = bc + (ln(a) - ln(p))c

    This equation allows us to calculate the temperature `t` given a specific vapor pressure `p`, using the constants `a, b, c, d` from the original equation.

    b * c + (a.ln() - p.ln()) * c
    <-- above was generated by phind.com ai and seems to be mistaken, since the result is crap.


    wolfram alpha query:
    solve p = a * e^((b - (t / c)) * (t / (d + t))) for t
    wolfram alpha solutions:
    t = 1/2 (-sqrt(c^2 (log(p/a) - b)^2 - (4 c d log(p/a))) - (c log(p/a)) + b c)
    t = 1/2 ( sqrt(c^2 (log(p/a) - b)^2 - (4 c d log(p/a))) - (c log(p/a)) + b c)

    wolfram alpha step by step:
          p = a *   ((b - (t / c)) * (t / (d + t))).exp()
    solve p = a * e^((b - (t / c)) * (t / (d + t))) for t

    1. reverse
          a *   ((b - (t / c)) * (t / (d + t))).exp() = p
    solve a * e^((b - (t / c)) * (t / (d + t))) = p for t

    2. divide both sides by a:
            ((b - (t / c)) * (t / (d + t))).exp() = p/a
    solve e^((b - (t / c)) * (t / (d + t))) = p/a for t

    3. apply natural logarithm to both sides:
    ((b - (t / c)) * (t / (d + t))) = (p/a).ln()
    solve ((b - (t / c)) * (t / (d + t))) = ln(p/a) for t

    4a. implicit step: multiply out the two fractions (b - (t / c) / 1) * (t / (d + t)) = (t * (b - (t / c))) / (d + t)
    (t * (b - (t / c))) / (d + t) = (p/a).ln()

    4. multiply both sides by (t+d)
    (t * (b - (t / c))) =  (p/a).ln() * (d + t)
    solve (t * (b - (t / c))) = ln(p/a) * (d + t) for t

    5. multiply out (t * (b - (t / c))) = t*b - t^2/c
    t*b - t^2/c = (p/a).ln() * (d + t)
    solve t*b - t^2/c = ln(p/a) * (d + t) for t

    6. expand ln(p/a) * (d + t) = d*ln(p/a) + t*ln(p/a)
    solve t*b - t^2/c = d*ln(p/a) + t*ln(p/a) for t

    7. move everything to the left:
    solve t*b - t^2/c - d*ln(p/a) - t*ln(p/a) = 0 for t

    8. write quadratic polynomial in standard form & collect in terms of t
    quadratic polynomial in standard form = a*x^2 + b*x + c = 0
    t^2/c = 1/c*t^2 ; t*b - t*ln(p/a) = t*(b - ln(p/a))
    -1/c*t^2 + t*(b - ln(p/a)) - d*ln(p/a) = 0

    9. apply quadratic formula:
    x = (-B ± (B^2 - 4*A*C).sqrt()) / 2*A
    -> A = -1/c ; B = (b - ln(p/a)) ; C = - d*ln(p/a)
    t = (-(b - ln(p/a)) ± ((b - ln(p/a))^2 - 4*(-1/c)*(-d*ln(p/a))).sqrt()) / (-2/c)

    10. simplify
    simplify t = (-(b - ln(p/a)) ± (sqrt((b - ln(p/a))^2 - 4*(-1/c)*(-d*ln(p/a))))) / (-2/c)
    t = -0.5 * c * (-(b - ln(p/a)) ± (sqrt((b - ln(p/a))^2 - 4*(-1/c)*(-d*ln(p/a)))))

    11. since we know ln(p/a) will stay for the final version, rewrite it as "g":
    t = -0.5 * c * (-(b - g) ± (sqrt((b - g)^2 - 4*(-1/c)*(-d*g))))

    */
    pub fn dewpoint_from_vapor_pressure_c_using_reverse_buck_equation(&self) -> f32 {
        let (a, b, c, d) = arden_buck_constants(false);
        let p = self.actual_water_vapor_pressure_pa();
        let g = (p / a).ln();
        let positive = -0.5 * c * (((b - g).powi(2) - (4.0 * d * g) / c).sqrt() + g - b);
        if positive > 0.0 {
            return positive;
        }

        let (a, b, c, d) = arden_buck_constants(true);
        let g = (p / a).ln();
        let negative = -0.5 * c * (((b - g).powi(2) - (4.0 * d * g) / c).sqrt() + g - b);
        if negative < 0.0 {
            negative
        } else {
            (positive + negative) / 2.0
        }

        //1.0/2.0 * ((c.powi(2) * g - b).powi(2) - 4.0 * c * d * g - c * g).sqrt()
        // 0.5 * ((c.powi(2) * (g - b).powi(2) - (4.0 * c * d * g)).sqrt() - (c * g) + b * c)
        // let x = (100.0*p/61121.0).ln();
        // (469.0).sqrt()*sqrt(117250000*(x + 2*i*π*n)^2 - 4894271000 (x) + 2 i π n) + 40904735949) - 469000 i π n - 234500 x) + 4379991)/2000
    }

    pub fn abs_humidity_g_per_m3(&self) -> f32 {
        /* AH = (RH × P) / (461.5 J/(kg⋅K) × T × 100)
        Where:
            AH is the absolute humidity in g/m³
            RH is the relative humidity as a decimal (e.g., 0.6 for 60%)
            P is the pressure in pascal (Pa)
            T is the temperature in Kelvin (K)

        \rho _{w}={\frac {e}{R_{w}\cdot T}}={\frac {m_{{{\text{Wasserdampf}}}}}{V_{{{\text{gesamt}}}}}}

        AH = Pₐ/(Rw × T)
        , where:
            AH — Absolute humidity, calculated in kilograms per cubic meter (kg/m³);
            Pₐ — Actual vapor pressure, in pascals (Pa);
            T — Air absolute temperature, in kelvin (K); and
            Rw = 461.5 J/(kg K) — Specific gas constant for water vapor;
        */
        self.actual_water_vapor_pressure_pa()
            / (GAS_CONSTANT_WATER_J_PER_G_K as f32 * self.temperature_k())
        // (self.rel_humidity_pct * self.pressure_pa) / (461.5 * self.temperature_k())
    }

    pub fn actual_water_vapor_pressure_pa(&self) -> f32 {
        /*
        Pₐ — Actual vapor pressure, in pascals (Pa);
        Pₐ​=Ps​×(RH/100)
        where:
            RH — Relative humidity, expressed as a percentage; and
            Ps​ — Saturation vapor pressure​, in Pa
        */
        self.saturated_water_vapor_pressure_pa() * self.rel_humidity_pct / 100.0
    }

    pub fn saturated_water_vapor_pressure_pa(&self) -> f32 {
        /*
        https://en.wikipedia.org/wiki/Vapour_pressure_of_water#Accuracy_of_different_formulations
        most accurate =
        https://en.wikipedia.org/wiki/Arden_Buck_equation
        */
        let t = self.temperature_c;
        if t.abs() < f32::EPSILON {
            return SATURATED_WATER_VAPOUR_0C_PA;
        }
        let (a, b, c, d) = arden_buck_constants(t < 0.0);
        a * ((b - (t / c)) * (t / (d + t))).exp()
    }

    pub fn temperature_k(&self) -> f32 {
        self.temperature_c + KELVIN_CELSIUS_DIFF
    }

    pub fn details(&self) -> String {
        format!(
            "{:.2}°C {:.2}Φ {:.2}g/m² TP{:.2}°C DP{:.2}°C AWP{:.0}Pa SWP{:.0}Pa Air{:.0}Pa",
            self.temperature_c,
            self.rel_humidity_pct,
            self.abs_humidity_g_per_m3(),
            self.dewpoint(),
            self.dewpoint_from_vapor_pressure_c_using_reverse_buck_equation(),
            self.actual_water_vapor_pressure_pa(),
            self.saturated_water_vapor_pressure_pa(),
            self.pressure_pa,
        )
        .replace('.', ",")
    }

    #[cfg(feature = "display")]
    pub fn scroll_text(&self) -> String {
        format!(
            "{:.2}°C {:.2}% TP{:.2}°C {:.2}°C ",
            self.temperature_c,
            self.rel_humidity_pct,
            self.dewpoint(),
            self.temperature_c
        )
        .replace('.', ",")
    }

    #[cfg(feature = "display")]
    pub fn get_variable(&self, variable: &MeasurementVariable) -> f32 {
        match variable {
            MeasurementVariable::Temperature => self.temperature_c,
            MeasurementVariable::HumidityRel => self.rel_humidity_pct,
            MeasurementVariable::HumidityAbs => self.abs_humidity_g_per_m3(),
            MeasurementVariable::PressureBar => self.pressure_pa / 100_000_f32,
            MeasurementVariable::DewPoint => self.dewpoint(),
            MeasurementVariable::DewPointB => {
                self.dewpoint_from_vapor_pressure_c_using_reverse_buck_equation() as f32
            }
        }
    }
}

impl fmt::Display for Measurement {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{:.02}°C {:.02}Φ {:.0}hPa",
            self.temperature_c, self.rel_humidity_pct, self.pressure_pa
        )
    }
}

fn send_measurements(m: &Measurement, tx: &Sender<String>) {
    let mac = APP_STATE.mac();

    tx.try_send(format!(",mac={mac} esp32.temperature={}", m.temperature_c))
        .or_else(|e| {
            warn!("bme280.temperature send failed: {e}");
            Err(e)
        })
        .ok();
    tx.try_send(format!(",mac={mac} esp32.pressure={}", m.pressure_pa))
        .or_else(|e| {
            warn!("bme280.pressure send failed: {e}");
            Err(e)
        })
        .ok();
    tx.try_send(format!(",mac={mac} esp32.humidity={}", m.rel_humidity_pct))
        .or_else(|e| {
            warn!("bme280.humidity send failed: {e}");
            Err(e)
        })
        .ok();
    tx.try_send(format!(",mac={mac} esp32.sensor_reads={}", m.sensor_reads))
        .or_else(|e| {
            warn!("bme280.sensor_reads send failed: {e}");
            Err(e)
        })
        .ok();
    tx.try_send(format!(
        ",mac={mac} esp32.avg={}",
        if m.avg { 1 } else { 0 }
    ))
    .or_else(|e| {
        warn!("bme280.avg send failed: {e}");
        Err(e)
    })
    .ok();
}

fn read_bme280_try(try_number: u8) -> Result<Measurement, Cow<'static, str>> {
    debug!("read_bme280_try starting");
    let mut lock = match APP_STATE.bme280_driver.try_write() {
        Some(lock) => lock,
        None => {
            return Err("couldn't acquire lock on bme280 driver store".into());
        }
    };
    let bme280 = match lock.as_mut() {
        Some(bme280) => bme280,
        None => {
            return Err("bme280 sensor is None".into());
        }
    };
    debug!("read_bme280_try: driver lock acquired");
    bme280
        .take_forced_measurement()
        .map_err(|e| format!("{:?}", e))?;
    match bme280.read_sample() {
        Ok((Some(temperature_c), Some(pressure_pa), Some(rel_humidity_pct))) => Ok(Measurement {
            temperature_c,
            pressure_pa,
            rel_humidity_pct,
            sensor_reads: try_number,
            avg: false,
        }),
        Ok(incomplete) => Err(format!(
            "bme280 gave incomplete measurement (will drop & retry!): {:?}",
            incomplete
        )
        .into()),
        Err(e) => Err(format!("{:?}", e).into()),
    }
    // .map_err(|e| format!("{:?}", e))
}

fn read_bme280_retry(max_tries: u8) -> Result<Measurement, Cow<'static, str>> {
    let mut tried: u8 = 1;
    let mut last_error = Cow::Borrowed("haven't tried, max_tries was 0");
    while tried <= max_tries {
        match read_bme280_try(tried) {
            Ok(bme280) => {
                log!(
                    DERIVED_CONFIG.log_bme280,
                    "unverified bme280 measurement: {bme280}"
                );
                APP_STATE
                    .bme280_read_time
                    .write()
                    .replace(unsafe { esp_timer_get_time() });
                return Ok(bme280);
            }
            Err(error) => {
                warn!("failed to read bme280 on try {tried}/{max_tries}, error={error}");
                if !error.starts_with("couldn't acquire lock on bme280 store") {
                    setup_bme280();
                }
                last_error = error;
            }
        }
        delay::FreeRtos::delay_ms(CONFIG.bme280_retry_ms);
        tried += 1;
    }
    Err(format!("read_bme280_retry: all {max_tries} tries failed! last error={last_error}").into())
}

fn bme280_plausible_diff(m1: &Measurement, m2: &Measurement, time_passed_sec: f32) -> bool {
    let (problems, _total_steps) = bme280_distance(m1, m2, time_passed_sec);
    problems.is_empty()
}

fn bme280_distance(m1: &Measurement, m2: &Measurement, time_passed_sec: f32) -> (Vec<String>, f32) {
    let mut problems: Vec<String> = Vec::new();
    if (m1.temperature_c - m2.temperature_c).abs()
        > CONFIG.expected_temperature_step * time_passed_sec
    {
        problems.push(format!(
            "bme280 temperature diff too big: {}°C vs {}°C = {}°C, expected in {}sec < {}°C",
            m1.temperature_c,
            m2.temperature_c,
            (m1.temperature_c - m2.temperature_c).abs(),
            time_passed_sec,
            CONFIG.expected_temperature_step * time_passed_sec
        ));
    }
    if (m1.rel_humidity_pct - m2.rel_humidity_pct).abs()
        > CONFIG.expected_humidity_step * time_passed_sec
    {
        problems.push(format!(
            "bme280 humidity diff too big: {}% vs {}% = {}%, expected in {}sec < {}%",
            m1.rel_humidity_pct,
            m2.rel_humidity_pct,
            (m1.rel_humidity_pct - m2.rel_humidity_pct).abs(),
            time_passed_sec,
            CONFIG.expected_humidity_step * time_passed_sec
        ));
    }
    if (m1.pressure_pa - m2.pressure_pa).abs() > CONFIG.expected_pressure_step * time_passed_sec {
        problems.push(format!(
            "bme280 pressure diff too big: {}Pa vs {}Pa = {}Pa, expected in {}sec < {}Pa",
            m1.pressure_pa,
            m2.pressure_pa,
            (m1.pressure_pa - m2.pressure_pa).abs(),
            time_passed_sec,
            CONFIG.expected_pressure_step * time_passed_sec
        ));
    }
    let total_steps = ((m1.temperature_c - m2.temperature_c).abs()
        / CONFIG.expected_temperature_step)
        + ((m1.rel_humidity_pct - m2.rel_humidity_pct).abs() / CONFIG.expected_humidity_step)
        + ((m1.pressure_pa - m2.pressure_pa).abs() / CONFIG.expected_pressure_step);
    (problems, total_steps)
}

fn read_bme280_validated_first(mut tries_left: u8) -> Result<Measurement, Cow<'static, str>> {
    let mut measurements: Vec<Measurement> = vec![];
    while tries_left > 0 {
        debug!("read_bme280_validated_first: getting measurement");
        let new = read_bme280_retry(tries_left)?;
        tries_left -= new.sensor_reads;
        for previous in measurements.iter() {
            debug!("read_bme280_validated_first: acquired at least 2 measurements, checking if two of them are plausible close together");
            if bme280_plausible_diff(previous, &new, 1_f32) {
                debug!("read_bme280_validated_first: got 2 plausible close measurements, returning their average: {previous} & {new}");
                return Ok(Measurement {
                    temperature_c: (previous.temperature_c + new.temperature_c) / 2.0,
                    rel_humidity_pct: (previous.rel_humidity_pct + new.rel_humidity_pct) / 2.0,
                    pressure_pa: (previous.pressure_pa + new.pressure_pa) / 2.0,
                    sensor_reads: measurements.len() as _,
                    avg: true,
                });
            }
        }
        measurements.push(new);
        delay::FreeRtos::delay_ms(CONFIG.bme280_retry_ms);
    }
    Err("read_bme280_validated_first: tries ran out".into())
}

fn read_bme280_validated_followed(
    max_tries: u8,
    last_accepted: &Measurement,
    last_time: i64,
) -> Result<Measurement, Cow<'static, str>> {
    let mut closest = Measurement {
        temperature_c: f32::MAX,
        pressure_pa: f32::MAX,
        rel_humidity_pct: f32::MAX,
        sensor_reads: 0,
        avg: false,
    };
    let mut average: Option<Measurement> = None;
    let mut read_num: f32 = 1_f32;
    let mut tries_left = max_tries;
    while tries_left > 0 {
        debug!("read_bme280_validated_followed: getting measurement");
        let new = read_bme280_retry(tries_left)?;
        let now: i64 = unsafe { esp_timer_get_time() };
        tries_left -= new.sensor_reads;
        closest.sensor_reads += new.sensor_reads;
        if (new.temperature_c - last_accepted.temperature_c).abs()
            < (closest.temperature_c - last_accepted.temperature_c).abs()
        {
            closest.temperature_c = new.temperature_c;
        }
        if (new.pressure_pa - last_accepted.pressure_pa).abs()
            < (closest.pressure_pa - last_accepted.pressure_pa).abs()
        {
            closest.pressure_pa = new.pressure_pa;
        }
        if (new.rel_humidity_pct - last_accepted.rel_humidity_pct).abs()
            < (closest.rel_humidity_pct - last_accepted.rel_humidity_pct).abs()
        {
            closest.rel_humidity_pct = new.rel_humidity_pct;
        }
        if let Some(avg) = &mut average {
            avg.temperature_c =
                avg.temperature_c / read_num * (read_num - 1.0) + new.temperature_c / read_num;
            avg.rel_humidity_pct = avg.rel_humidity_pct / read_num * (read_num - 1.0)
                + new.rel_humidity_pct / read_num;
            avg.pressure_pa =
                avg.pressure_pa / read_num * (read_num - 1.0) + new.pressure_pa / read_num;
            avg.sensor_reads += new.sensor_reads;
            avg.avg = true;
        }
        if average.is_none() {
            average.replace(new);
        }
        let avg = average.as_ref().unwrap();
        if tries_left <= 0 {
            read_num *= 2.0; // be twice as forgiving with our last try
        }
        let (problems_avg, steps_avg) = bme280_distance(
            avg,
            last_accepted,
            read_num * (now - last_time) as f32 / 1e+6_f32,
        );
        let (problems_last, steps_last) = bme280_distance(
            &closest,
            last_accepted,
            read_num * (now - last_time) as f32 / 1e+6_f32,
        );
        if problems_avg.is_empty() && problems_last.is_empty() && steps_last < steps_avg {
            return Ok(closest);
        }
        if problems_avg.is_empty() {
            return Ok(average.unwrap());
        }
        if problems_last.is_empty() {
            return Ok(closest);
        }
        if tries_left <= 0 {
            return Err(format!(
                "read_bme280_validated_followed: couldn't acquire close enough measurement with {max_tries} tries! new={new}, closest={closest}, last_accepted={last_accepted}, seconds_since={}, factor={read_num}, problems: {}",
                (now - last_time) as f32 / 1e+6_f32,
                problems_last.join("; ")
            )
            .into());
        } else {
            log!(
                DERIVED_CONFIG.log_bme280_dropped,
                "bme280 measurement dropped (try {}/{max_tries}): new={new}, closest={closest}, last_accepted={last_accepted}, seconds_since={}, factor={read_num}, problems: {}",
                max_tries-tries_left,
                (now - last_time) as f32 / 1e+6_f32,
                problems_last.join("; ")
            );
        }
        delay::FreeRtos::delay_ms(CONFIG.bme280_retry_ms);
        read_num += 1_f32;
    }
    Err(format!("read_bme280_validated_followed: called with {max_tries}").into())
}

pub fn read_and_send_bme280(tx: &Sender<String>) -> Result<(), String> {
    let last_accepted = APP_STATE.bme280_last_block();
    let last_time = *APP_STATE.bme280_time.read();

    let tries_left = ((CONFIG.bme280_period_ms / CONFIG.bme280_retry_ms) - 2) as u8;
    let validated = match (last_accepted, last_time) {
        (Some(last_accepted), Some(last_time)) => {
            read_bme280_validated_followed(tries_left, &last_accepted, last_time)
        }
        _ => read_bme280_validated_first(tries_left),
    };
    match validated {
        Ok(validated) => {
            log!(
                DERIVED_CONFIG.log_bme280,
                "verified bme280 measurement: {validated}"
            );
            send_measurements(&validated, tx);
            APP_STATE.bme280_push(validated);
            Ok(())
        }
        Err(e) => {
            APP_STATE.bme280_failed();
            Err(e.into())
        }
    }
}

pub fn setup_bme280() {
    let i2c_driver = match APP_STATE.i2c() {
        Some(i2c) => i2c,
        None => {
            error!("setup_bme280: couldn't acquire i2c_proxy!");
            return;
        }
    };
    info!("setup BME280 now");
    let mut bme280 = Bme280::new(i2c_driver, delay::FreeRtos);
    trace!("created bme280 driver, next try init");

    if let Err(error) = bme280.init() {
        error!("failed to init bme280 driver: {:?}", error);
        return;
    }
    if let Err(error) = bme280.set_sampling_configuration(
        bme280_rs::Configuration::default()
            .with_temperature_oversampling(Oversampling::Oversample1)
            .with_pressure_oversampling(Oversampling::Oversample1)
            .with_humidity_oversampling(Oversampling::Oversample1)
            .with_sensor_mode(SensorMode::Forced),
    ) {
        error!("failed to set bme280 sampling_configuration: {:?}", error);
        return;
    }
    trace!("setup_bme280: driver initialized");
    match APP_STATE.bme280_driver.try_write() {
        Some(mut lock) => {
            lock.replace(bme280);
        }
        None => error!("couldn't acquire lock on bme280 store"),
    }
    trace!("setup_bme280: stored to APP_STATE");
}

pub fn bme280_freeze_detector(tx: &Sender<String>) {
    let now: i64 = unsafe { esp_timer_get_time() };
    if now > CONFIG.min_uptime_before_reboot_sec * 1000 * 1000 {
        let mut reboot_reason = None;
        match APP_STATE.bme280_read_time.try_read() {
            Some(lock) => match lock.as_ref() {
                Some(read_time) => {
                    if (now - read_time) as u32
                        > 1000 * CONFIG.bme280_period_ms * CONFIG.bme280_allowed_skips
                    {
                        error!(
                            "last successful bme280 sensor read {} ago. allowed_skips={}, bme280_period_ms={}, allowed={} -> rebooting!",
                            format_duration(Duration::from_micros((now - read_time) as _)),
                            CONFIG.bme280_allowed_skips,
                            CONFIG.bme280_period_ms,
                            format_duration(Duration::from_millis((CONFIG.bme280_period_ms * CONFIG.bme280_allowed_skips) as _))
                        );
                        reboot_reason.replace("bme280-freeze");
                    }
                }
                None => {
                    error!("bme280 setup failed, try rebooting since CONFIG.min_uptime_before_reboot_sec has been surpassed");
                    reboot_reason.replace("bme280-failed-start");
                }
            },
            None => {
                info!("bme280_freeze_detector: APP_STATE.bme280_last.try_read() failed (that means the reader thread currently writes back a new value?)");
            }
        };
        if let Some(reason) = reboot_reason {
            match APP_STATE.ota_time.try_read() {
                Some(lock) => {
                    if let Some(time) = lock.as_ref()
                        && (1000 * 1000 * 60 * 5) > (now - time) as u32
                    {
                        warn!("skipping bme280_freeze detection reboot, because ota started less than 5 minutes ago!");
                        return;
                    }
                }
                None => error!("ota_time unknown, couldn't acquire lock."),
            }
            tx.try_send(format!(
                ",mac={},resetReason={reason} esp32.uptime=0",
                APP_STATE.mac()
            ))
            .ok();
            APP_STATE.schedule(Duration::from_secs(1), false, || {
                warn!("bme280 not working! reboot to try again!");
                unsafe { esp_restart() };
            });
        }
    }
}

pub fn bme280_loop(tx: Sender<String>) {
    info!("bme280_loop started");
    loop {
        if let Err(e) = read_and_send_bme280(&tx) {
            error!("read_and_send_bme280 failed! {e}");
        };
        delay::FreeRtos::delay_ms(CONFIG.bme280_period_ms);
    }
}
