use crate::{
    app_state::{start_thread, APP_STATE},
    config::{escaped_value, CONFIG, DERIVED_CONFIG},
    log_buffer::LOG_BUFFER,
};
use crossbeam_channel::{bounded, Receiver, Sender};
use esp_idf_svc::hal::delay;
use esp_idf_svc::sys::{esp_app_desc, va_list};
use log::{info, Level};
use once_cell::sync::OnceCell;
use printf_compat::{format, output};
use std::{
    borrow::Cow, ffi::{c_char, c_int, CStr, VaList}, io
};

#[derive(Debug)]
enum Signal {
    Send,
    AddAndSend(String, Level),
}
pub struct BufferedLogger {
    sender: OnceCell<Sender<Signal>>,
}

impl BufferedLogger {
    const fn instance() -> Self {
        Self {
            sender: OnceCell::new(),
        }
    }

    pub fn store_and_send(&self, msg: String, level: Level) {
        // match LOG_BUFFER.try_write() {
        //     Some(mut buffer) => {
        //         if let Err(error) = buffer.push_msg(&msg, level) {
        //             eprintln!("syslog_process failed, sending later. error: {error}");
        //             self.send_later(Signal::Send);
        //         }
        //     }
        //     None => self.send_later(Signal::AddAndSend(msg, level)),
        // }
        self.send_later(Signal::AddAndSend(msg, level));
    }

    fn init_sender() -> Result<Sender<Signal>, io::Error> {
        println!("send_later called for the first time, setting up channel + receiver loop thread");
        let (tx, rx) = bounded(128);
        let tx_clone = tx.clone();
        start_thread("delayed_log\0", 1, CONFIG.stack_size_logger, move || {
            receiver_loop(rx, tx)
        });
        Ok(tx_clone)
    }

    fn send_later(&self, signal: Signal) {
        // we can't use any log macro here, because it would be a recursive endless loop (until the memory for the call stack runs out..)
        // println!("BufferedLogger.buffer is busy, send_later: {:?}", signal);

        let sender = match self.sender.get_or_try_init(Self::init_sender) {
            Ok(sender) => sender,
            Err(error) => {
                eprintln!(
                    "Failed to init or retrieve syslog sender, dropping Signal={:?}, error={error}",
                    signal
                );
                return;
            }
        };

        if let Err(error) = sender.try_send(signal) {
            eprintln!("BufferedLogger.send_later: failed to add signal to queue! error={error}");
        }
    }
}

fn receiver_loop(rx: Receiver<Signal>, tx: Sender<Signal>) {
    loop {
        // println!("buffered_logger.receiver_loop: loop start");
        let signal = match rx.recv() {
            Ok(signal) => signal,
            Err(error) => {
                eprintln!("couldn't receive message from queue! {error}");
                continue;
            }
        };
        println!("buffered_logger.receiver_loop: received {:?}", signal);

        if let Err(error) = match signal {
            Signal::Send => LOG_BUFFER.write().syslog_process(),
            Signal::AddAndSend(msg, level) => LOG_BUFFER.write().push_msg(&msg, level),
        } {
            eprintln!("LogBuffer action failed! error={error}");
            LOG_BUFFER.write().syslog_stream.take();

            if rx.is_empty() {
                println!(
                    "syslog_process failed & receiver is empty. wait a bit, then retry with newly added Send Signal"
                );
                delay::FreeRtos::delay_ms(1000);
                tx.try_send(Signal::Send).ok();
            }
        }
        // println!("buffered_logger.receiver_loop: loop end");
    }
}

impl log::Log for BufferedLogger {
    fn enabled(&self, metadata: &log::Metadata) -> bool {
        metadata.level() <= DERIVED_CONFIG.log_max_level
    }

    fn log(&self, record: &log::Record) {
        if self.enabled(record.metadata()) {
            let marker = log_marker(record.level());
            let mut module = record.module_path().unwrap_or("unknown");
            let mut file = record.file().unwrap_or("");
            let mut file_out = Cow::from(file);
            let line = record.line().unwrap_or(0);
            let raw_msg = record.args();
            let raw_str = format!("{raw_msg}");
            let idf = raw_str.starts_with("esp-idf: ");

            file = file.strip_prefix(CONFIG.log_strip_prefix).unwrap_or(file);
            if let Some(src_idx) = file.find("src/") {
                let (pre_src, with_src) = file.split_at(src_idx);
                let mut no_src = &with_src[3..]; // remove only src, leave the / after
                let no_ext = no_src.strip_suffix(".rs").unwrap_or(no_src);
                let module_path = no_ext.replace("/", "::");
                module = module.strip_suffix(&module_path).unwrap_or(module);
                no_src = &no_src[1..]; // remove the / after src
                if pre_src.is_empty() {
                    file_out = Cow::Borrowed(no_src);
                }
                else {
                    file_out = Cow::Owned(format!("{pre_src}{no_src}"))
                }
            }
            let msg = match log_color(record.level()) {
                Some(color) => {
                    format!("\x1b[0;{color}m{marker} [{module}] ({file_out}:{line}) {raw_msg}\x1b[0m")
                }
                None => format!("{marker} [{module}] ({file_out}:{line}) {raw_msg}"),
            };
            if !idf {
                println!("{msg}")
            };
            self.store_and_send(msg, record.level());
        }
    }

    fn flush(&self) {}
}

#[inline]
const fn log_marker(level: Level) -> &'static str {
    match level {
        Level::Error => "E",
        Level::Warn => "W",
        Level::Info => "I",
        Level::Debug => "D",
        Level::Trace => "V",
    }
}

#[inline]
const fn log_color(level: Level) -> Option<u8> {
    match level {
        // for a list of available colors see: https://talyian.github.io/ansicolors/
        Level::Error => Some(31), // red
        Level::Warn => Some(33),  // yellow / brown
        Level::Info => Some(32),  // green
        Level::Debug => Some(36), // 36 = cyan
        Level::Trace => Some(34), // 34 = blue
        // 2 = dim (doesn't work everywhere)
                                   // _ => None,
    }
}

#[allow(improper_ctypes_definitions)]
pub unsafe extern "C" fn esp_log_processor(pattern: *const c_char, args: va_list) -> c_int {
    if *APP_STATE.esp_log_vararg_parsing.read() {
        let args_transmuted: VaList = core::mem::transmute(args);
        let mut out = String::new();
        format(pattern, args_transmuted, output::fmt_write(&mut out));
        let trimmed = out.trim();
        if !trimmed.is_empty() {
            info!("esp-idf: {trimmed}");
        }
    } else {
        if let Some(default_esp_logger) = APP_STATE.default_esp_logger.get() {
            unsafe { default_esp_logger(pattern, args) };
        }
        let pattern_cstr = CStr::from_ptr(pattern);
        // we can't iterate over args anymore! args_hex is useless anyway, so only print the pattern in fallback mode!
        // let args_hex: String = args.iter().map(|&x| format!("{:x}", x)).collect();
        BUFFERED_LOGGER.store_and_send(
            format!("esp-idf: {:?}", pattern_cstr),
            Level::Info, // match args[2] {
                         //     16 => Level::Info,
                         //     12 => Level::Debug,
                         //     _ => Level::Warn,
                         // },
        );
    }
    0
}

pub fn log_config() {
    let config = format!("{:?}", CONFIG)
        .replace(escaped_value(CONFIG.wifi_psk).as_ref(), "**hidden**")
        .replace(escaped_value(CONFIG.wifi_ap_pwd).as_ref(), "**hidden**");
    info!("{config}");
}

pub fn log_compile_info() {
    esp_app_desc!();

    // WARNING: Bug workaround: time & date are flipped!
    let date = unsafe { CStr::from_ptr(esp_app_desc.time.as_ptr()) }.to_string_lossy();
    let time = unsafe { CStr::from_ptr(esp_app_desc.date.as_ptr()) }.to_string_lossy();

    let name = unsafe { CStr::from_ptr(esp_app_desc.project_name.as_ptr()) }.to_string_lossy();
    let version = unsafe { CStr::from_ptr(esp_app_desc.version.as_ptr()) }.to_string_lossy();
    let idf = unsafe { CStr::from_ptr(esp_app_desc.idf_ver.as_ptr()) }.to_string_lossy();

    info!("Running {name} v{version} @ IDF v{idf} - compiled {date} {time}");
}

pub static BUFFERED_LOGGER: BufferedLogger = BufferedLogger::instance();
