use esp_idf_svc::sys::esp_timer_get_time;
use log::{error, info, Level};
use parking_lot::RwLock;

use std::{
    cmp::Ordering::*,
    io::{self, Write},
    net::TcpStream,
    str::FromStr,
    time::Duration,
};

use crate::{app_state::APP_STATE, human_readable_duration::format_duration};
use crate::{config::CONFIG, hostname_socket_addr::HostnameSocketAddr};

pub struct LogBuffer {
    buffer: [u8; CONFIG.log_buffer_size],
    last_sent: usize,
    write_position: usize,
    overflow: bool,
    filled: bool,
    // line: bool,
    syslog_target: Option<HostnameSocketAddr>,
    pub syslog_stream: Option<TcpStream>,
    syslog_connecting: bool,
    syslog_last_connection_attempt: Option<i64>,
    problems: [u8; CONFIG.warn_buffer_size],
    problems_write: usize,
    problems_filled: bool,
}

fn syslog_prefix(stream: &mut TcpStream) -> Result<(), io::Error> {
    stream.write_all(b"ESP32 ")?;
    stream.write_all(APP_STATE.mac().as_bytes())?;
    stream.write_all(b" ")
}

fn syslog_send(stream: &mut TcpStream, bytes: &[u8]) -> Result<(), io::Error> {
    for byte in bytes {
        // we can write each byte on it's own, the TcpStream does buffer before sending (unless TCP_NODELAY has been set to true)
        stream.write_all(&[*byte])?;
        if *byte == b'\n' {
            syslog_prefix(stream)?;
        }
    }
    Ok(())
}

impl LogBuffer {
    const OVERFLOW_WARNING: &'static [u8] = b"WARNING: buffer overflow happened, some msgs got lost!\n";

    const fn instance() -> Self {
        Self {
            buffer: [0; CONFIG.log_buffer_size],
            last_sent: 0,
            write_position: 0,
            overflow: false,
            filled: false,
            // line: false,
            syslog_target: None,
            syslog_stream: None,
            syslog_connecting: false,
            syslog_last_connection_attempt: None,
            problems: [0; CONFIG.warn_buffer_size],
            problems_write: 0,
            problems_filled: false,
        }
    }

    pub fn syslog_init(&mut self) {
        info!("syslog_init start");
        match HostnameSocketAddr::from_str(CONFIG.syslog_target) {
            Ok(h) => {
                info!("syslog_target parsed successfully: {h}");
                self.syslog_target.replace(h);
            }
            Err(err) => {
                error!(
                    "couldn't parse or resolve syslog_target: '{}' - error: {err}",
                    CONFIG.syslog_target
                );
            }
        };
    }

    fn syslog_connect(&mut self) -> Result<(), io::Error> {
        println!("LogBuffer.syslog connecting");
        if let Some(time) = self.syslog_last_connection_attempt {
            let passed = unsafe { esp_timer_get_time() } - time;
            let duration = Duration::from_micros(passed as _);
            if passed < 3 * 1000 * 1000 {
                return Err(io::Error::new(
                    io::ErrorKind::InvalidInput,
                    format!(
                        "last syslog connection attempt started {} ago, not retrying that soon.",
                        format_duration(duration)
                    ),
                ));
            }
        }
        self.syslog_last_connection_attempt
            .replace(unsafe { esp_timer_get_time() });

        let mut new_stream = match &self.syslog_target {
            Some(target) => {
                target.connect_timeout(Duration::from_millis(CONFIG.influxdb_connection_timeout_ms.into()))?
            }
            None => {
                return Err(io::Error::new(
                    io::ErrorKind::InvalidInput,
                    "can't connect, because target initialization failed or hasn't happened yet!",
                ));
            }
        };
        println!("LogBuffer.syslog connected");
        // new_stream.set_nodelay(false)?; // Protocol not available (os error 109) // but it still looks like it does buffer and not send each bytes on it's own
        syslog_prefix(&mut new_stream)?;
        println!("LogBuffer.syslog connected & prefix sent");
        self.syslog_stream.replace(new_stream);
        Ok(())
    }

    pub fn syslog_process(&mut self) -> Result<(), io::Error> {
        if self.syslog_connecting {
            // don't call the log macros recursively
            println!("other thread is currently connecting to syslog, they'll do the work of sending too and include my msg in the process. returning Ok(())");
            return Ok(());
        } else if self.syslog_stream.is_none() {
            // try reconnecting, returning error if it fails -> the "?" operator
            self.syslog_connecting = true;
            let connected: Result<(), io::Error> = self.syslog_connect();
            self.syslog_connecting = false;
            connected?;
        };
        if let Some(stream) = self.syslog_stream.as_mut() {
            // try send
            // trace!("LogBuffer.syslog got stream, sending now");
            match self.last_sent.cmp(&self.write_position) {
                Less => syslog_send(stream, &self.buffer[self.last_sent..self.write_position])?,
                Greater => {
                    if self.overflow {
                        syslog_send(stream, Self::OVERFLOW_WARNING)?;
                    }
                    syslog_send(stream, &self.buffer[self.last_sent..])?;
                    syslog_send(stream, &self.buffer[..self.write_position])?;
                }
                Equal => (),
            };
            self.overflow = false;
            self.last_sent = self.write_position;
            Ok(())
        } else {
            // the only reason we could land here, is if syslog_target.is_none(),
            // because if only syslog_stream.is_none() we would have tried reconnecting
            // and if connecting failed, that error would have already been returned
            // by the "?" operator in the first if
            Err(io::Error::new(
                io::ErrorKind::InvalidInput,
                "no (valid) syslog target server specified",
            ))
        }
    }

    pub fn push_fast(&mut self, byte: u8) {
        if self.last_sent == (self.write_position + 1) {
            self.overflow = true;
            self.last_sent += 1;
        }
        // println!("log_buffer[{}]={}", self.write_position, byte as char);
        self.buffer[self.write_position] = byte;
        self.write_position = (self.write_position + 1) % self.buffer.len();

        if !self.filled && self.write_position == self.buffer.len() - 1 {
            self.filled = true;
        }
        // if byte == b'\n' {
        //     self.line = true;
        // }
    }

    pub fn push(&mut self, byte: u8) -> Result<(), io::Error> {
        self.push_fast(byte);
        self.syslog_process()
    }

    pub fn push_all_fast(&mut self, bytes: &[u8]) {
        for &byte in bytes {
            self.push_fast(byte);
        }
    }

    // pub fn push_all(&mut self, bytes: &[u8]) {
    //     for &byte in &bytes[..=bytes.len() - 2] {
    //         self.push_fast(byte);
    //     }
    //     if let Some(&last_byte) = bytes.last() {
    //         self.push(last_byte);
    //     }
    // }

    pub fn push_line(&mut self, line: &str) -> Result<(), io::Error> {
        self.push_all_fast(line.as_bytes());
        self.push(b'\n')
    }

    pub fn push_msg(&mut self, line: &str, level: Level) -> Result<(), io::Error> {
        if level <= Level::Warn {
            self.push_problem(line);
        }
        self.push_line(line)
    }

    pub fn read_all<'a>(&'a self) -> Box<dyn Iterator<Item = &'a u8> + 'a> {
        if self.filled {
            let part1 = &self.buffer[self.write_position..];
            let part2 = &self.buffer[..self.write_position];
            info!(
                "CustomBuffer.read_all - part1.len={}; part2.len={}",
                part1.len(),
                part2.len()
            );
            Box::new(part1.iter().chain(part2))
        } else {
            info!(
                "CustomBuffer.read_all not yet filled, last_written={}",
                self.write_position
            );
            Box::new(self.buffer[0..self.write_position].iter())
        }
    }

    pub fn push_problem_byte(&mut self, byte: u8) {
        self.problems[self.problems_write] = byte;
        self.problems_write = (self.problems_write + 1) % self.problems.len();

        if !self.problems_filled && self.problems_write == self.buffer.len() - 1 {
            self.problems_filled = true;
        }
    }

    pub fn push_problem(&mut self, line: &str) {
        for &byte in line.as_bytes() {
            self.push_problem_byte(byte);
        }
        self.push_problem_byte(b'\n');
    }

    pub fn read_problems<'a>(&'a self) -> Box<dyn Iterator<Item = &'a u8> + 'a> {
        if self.problems_filled {
            let part1 = &self.problems[self.problems_write..];
            let part2 = &self.problems[..self.problems_write];
            Box::new(part1.iter().chain(part2))
        } else {
            Box::new(self.problems[0..self.problems_write].iter())
        }
    }

    // pub fn read_unread<'a>(&'a mut self) -> Box<dyn Iterator<Item = &'a u8> + 'a> {
    //     let result: Box<dyn Iterator<Item = &u8>> = if self.overflow {
    //         let part1 = &self.buffer[self.write_position..];
    //         let part2 = &self.buffer[..self.write_position];
    //         Box::new(Self::OVERFLOW_WARNING.iter().chain(part1).chain(part2))
    //     } else if self.write_position >= self.last_sent {
    //         Box::new(self.buffer[self.last_sent..self.write_position].iter())
    //     } else {
    //         let part1 = &self.buffer[self.last_sent..];
    //         let part2 = &self.buffer[..self.write_position];
    //         Box::new(part1.iter().chain(part2))
    //     };
    //     self.last_sent = self.write_position;
    //     // self.line = false;
    //     self.overflow = false;
    //     result
    // }
}

pub static LOG_BUFFER: RwLock<LogBuffer> = RwLock::new(LogBuffer::instance());
