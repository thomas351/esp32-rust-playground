use crate::{
    app_state::{start_thread, APP_STATE},
    bme280_handler::MeasurementVariable,
    config::CONFIG,
    human_readable_duration::format_duration,
};
use anyhow::anyhow;
use crossbeam_channel::{bounded, Receiver, Sender};
use embedded_graphics::{
    pixelcolor::BinaryColor,
    prelude::{Point, Size},
    primitives::{Primitive, PrimitiveStyle, Rectangle},
    Drawable,
};
use esp_idf_svc::hal::{delay,i2c::I2cDriver};
use esp_idf_svc::sys::esp_timer_get_time;
use log::{debug, error, info, trace, warn};
use once_cell::sync::OnceCell;
use parking_lot::RwLock;
use ssd1306::{
    prelude::DisplayConfig, rotation::DisplayRotation, size::DisplaySize128x32,
    I2CDisplayInterface, Ssd1306,
};
use std::time::Duration;
use strum_macros::{Display, EnumIter, EnumString};
use u8g2_fonts::{
    fonts::{u8g2_font_crox2t_tn, u8g2_font_logisoso16_tf, u8g2_font_logisoso28_tf},
    types::{FontColor, RenderedDimensions, VerticalPosition},
    FontRenderer,
};

pub type DisplayDriver = Ssd1306<
    ssd1306::prelude::I2CInterface<
        shared_bus::I2cProxy<'static, std::sync::Mutex<I2cDriver<'static>>>,
    >,
    DisplaySize128x32,
    ssd1306::mode::BufferedGraphicsMode<DisplaySize128x32>,
>;

#[derive(PartialEq, Debug)]
pub enum DisplayState {
    Scroll,
    History,
    Timer { start: i64 },
    Touch,
    Paused,
    Off,
}

#[derive(PartialEq, Debug, EnumIter, EnumString, Clone, Copy, Display)]
pub enum DisplayStateChange {
    Scroll,
    History,
    Toggle,
    Uptime,
    Timer,
    TimerToggle,
    Touch,
    Plus,
    Minus,
    Pause,
    Fill,
    Dimensions,
    Clear,
    RelayToggle,
}

pub struct DisplayHandler {
    pub display: RwLock<Option<DisplayDriver>>,
    pub mode_selector: OnceCell<Sender<DisplayStateChange>>,
    font_big: FontRenderer,
    font_small: FontRenderer,
    font_tiny: FontRenderer,
}

impl DisplayHandler {
    const fn instance() -> Self {
        DisplayHandler {
            display: RwLock::new(None),
            mode_selector: OnceCell::new(),
            font_big: FontRenderer::new::<u8g2_font_logisoso28_tf>(),
            font_small: FontRenderer::new::<u8g2_font_logisoso16_tf>(),
            font_tiny: FontRenderer::new::<u8g2_font_crox2t_tn>(),
        }
    }

    pub fn display_action<F>(&self, action: F) -> anyhow::Result<()>
    where
        F: FnOnce(&mut DisplayDriver) -> anyhow::Result<()>,
    {
        action(
            self.display
                .try_write()
                .ok_or(anyhow!(
                    "DisplayHandler.display.try_write failed to acquire lock!"
                ))?
                .as_mut()
                .ok_or(anyhow!(
                    "DisplayHandler.display contained None! Not yet initialized?!"
                ))?,
        )
    }

    pub fn setup_display_thread(&self) -> anyhow::Result<()> {
        info!("setup_display_thread start");

        let (tx, rx) = bounded::<DisplayStateChange>(2);
        self.mode_selector.set(tx).ok();

        start_thread("display\0", 20, CONFIG.stack_size_display, move || {
            DISPLAY_HANDLER.setup_display();
            DISPLAY_HANDLER.i2c_display_loop(rx);
        });

        info!("setup_display_thread finished");
        Ok(())
    }

    fn change_state(&self, old_state: DisplayState, change: DisplayStateChange) -> DisplayState {
        info!(
            "display state is {:?} change requested: {:?}",
            old_state, change
        );

        if old_state == DisplayState::Touch && change != DisplayStateChange::Touch {
            let mut guard = APP_STATE.display_touches.write();
            *guard = false;
        }

        let simple_change: Option<DisplayState> = match change {
            DisplayStateChange::Scroll => Some(DisplayState::Scroll),
            DisplayStateChange::History => Some(DisplayState::History),
            DisplayStateChange::Timer => Some(DisplayState::Timer {
                start: unsafe { esp_timer_get_time() },
            }),
            DisplayStateChange::Uptime => Some(DisplayState::Timer { start: 0 }),
            DisplayStateChange::TimerToggle => Some(match old_state {
                DisplayState::Timer { start: 0 } => DisplayState::Timer {
                    start: unsafe { esp_timer_get_time() },
                },
                DisplayState::Timer { .. } => DisplayState::Paused,
                _ => DisplayState::Timer { start: 0 },
            }),
            DisplayStateChange::Pause => Some(DisplayState::Paused),
            DisplayStateChange::Toggle => match old_state {
                DisplayState::Off | DisplayState::Paused => Some(DisplayState::Scroll),
                DisplayState::Scroll => Some(DisplayState::History),
                _ => None, // for all other current_states: Toggle = Clear
            },
            DisplayStateChange::Minus | DisplayStateChange::Plus => Some(old_state),
            DisplayStateChange::Touch => {
                let mut guard = APP_STATE.display_touches.write();
                *guard = true;
                Some(DisplayState::Touch)
            }
            DisplayStateChange::RelayToggle => {
                match APP_STATE.relay_toggle(None) {
                    Ok(state) => info!("relay state was {}, toggled to {}", !state, state),
                    Err(error) => error!("relay toggle failed! {error}"),
                };
                Some(old_state)
            }
            _ => None,
        };
        if let Some(new_state) = simple_change {
            return new_state;
        }

        if let Err(error) = self.display_action(|display| {
            match change {
                DisplayStateChange::Clear | DisplayStateChange::Toggle => {
                    display.clear_buffer();
                }
                DisplayStateChange::Fill => {
                    display_fill(display);
                }
                DisplayStateChange::Dimensions => {
                    display_dimensions(display);
                }
                _ => (),
            };
            display.flush().map_err(|e| anyhow!("{e:?}"))
        }) {
            error!("display change '{change}' failed: {error}");
        };
        match change {
            DisplayStateChange::Clear | DisplayStateChange::Toggle => DisplayState::Off,
            DisplayStateChange::Pause => DisplayState::Paused,
            _ => {
                error!("unexpected display state-change: {}", change);
                DisplayState::Paused
            }
        }
    }

    fn i2c_display_loop(&self, rx: Receiver<DisplayStateChange>) {
        info!("i2c_display_loop started");
        let mut bme280_format: Option<String> = None;
        let mut display_shift = 0;
        let mut display_speed = 4;
        let mut current_state = DisplayState::Scroll;
        let mut variable = MeasurementVariable::Temperature;

        let mut timeout_ms = 10_u64;
        let mut collisions: Vec<(u32, u32, bool)> = vec![];
        let mut remaining_blinks = 0_u8;

        let mut last_change_time = unsafe { esp_timer_get_time() };
        let mut flash_change_time = unsafe { esp_timer_get_time() };
        let mut flash_shown = false;
        let mut relay_active: bool;

        loop {
            let maybe_change = match current_state {
                DisplayState::Off => rx.recv()
                    .map_err(|e| error!("display_handler.i2c_display_loop rx.recv() failed: {e}")),
                _ => rx.recv_timeout(Duration::from_millis(timeout_ms))
                    .map_err(|e|debug!("display_handler.i2c_display_loop rx.recv_timeout() failed, I believe this only happens if channel is and stayed empty for given timeout = no change happened. error={e}")),
            };
            if let Ok(change) = maybe_change {
                current_state = self.change_state(current_state, change.clone());
                if current_state == DisplayState::Scroll {
                    let speed_step = 1.max((0.1 * display_speed as f32) as _);
                    display_speed = match change {
                        DisplayStateChange::Plus => display_speed + speed_step,
                        DisplayStateChange::Minus => 1.max(display_speed - speed_step),
                        _ => display_speed,
                    };
                } else if current_state == DisplayState::History
                    && change == DisplayStateChange::Minus
                {
                    variable = MeasurementVariable::progress_variable(variable, -2);
                }
                remaining_blinks = 0;
                last_change_time = unsafe { esp_timer_get_time() };
            }
            relay_active = *APP_STATE.relay_active.read();
            match current_state {
                DisplayState::Off | DisplayState::Paused => {
                    info!("display scroll state is paused, waiting for state change signal");
                    timeout_ms = CONFIG.clear_display_after_secs * 1000;
                }
                DisplayState::Touch => {
                    trace!("display showing touches, controlled by app_state. display_loop waiting for change signal");
                    timeout_ms = 2000;
                }
                DisplayState::History => {
                    if remaining_blinks == 0 {
                        collisions.clear();
                        self.display_next_history(&mut variable, &mut collisions);
                        timeout_ms = match collisions.is_empty() && !relay_active {
                            true => 1500,
                            false => {
                                remaining_blinks = 15;
                                100
                            }
                        }
                    } else {
                        if let Err(error) = self.display_action(|display| {
                            for (x, y, missing) in &collisions {
                                if *missing {
                                    for i in 0..31 {
                                        display.toggle_pixel(*x, i);
                                    }
                                } else {
                                    display.toggle_pixel(*x, *y);
                                }
                            }
                            Ok(())
                        }) {
                            error!("history collision pixel toggling failed: {error}");
                        };
                        remaining_blinks -= 1;
                    }
                }
                DisplayState::Timer { start } => {
                    self.display_timer(&start);
                    timeout_ms = 10;
                }
                DisplayState::Scroll => {
                    timeout_ms = match self.display_scroll(
                        &mut bme280_format,
                        &mut display_shift,
                        display_speed,
                    ) {
                        true => 10,
                        false => 100,
                    };
                }
            }

            if current_state != DisplayState::Off {
                if relay_active {
                    let now = unsafe { esp_timer_get_time() };
                    if now - flash_change_time >= 1000 * 100
                        || current_state == DisplayState::History
                    {
                        flash_shown = !flash_shown;
                        flash_change_time = now;
                    }
                    if flash_shown {
                        self.flash_toggle();
                    }
                }
                self.display_action(|display| display.flush().map_err(|e| anyhow!("{e:?}")))
                    .map_err(|e| error!("display.flush failed: {e}"))
                    .ok();
                if (unsafe { esp_timer_get_time() } - last_change_time)
                    > CONFIG.clear_display_after_secs as i64 * 1_000_000
                {
                    if let Some(selector) = self.mode_selector.get() {
                        if let Err(error) = selector.send(DisplayStateChange::Clear) {
                            error!(
                            "i2c_display_loop: couldn't clear display after 'clear_display_after_secs' passed! {error}"
                        )
                        } else {
                            info!("i2c_display_loop: clearing display because 'clear_display_after_secs' time ran out");
                        }
                    } else {
                        error!("i2c_display_loop: failed to acquire mode_selector sender from OnceCell for clearing screen after 'clear_display_after_secs' time ran out");
                    }
                }
            }
        }
    }

    fn setup_display(&self) {
        info!("setup_display start");

        let i2c = match APP_STATE.i2c() {
            Some(i2c) => i2c,
            None => {
                error!("setup_display: couldn't acquire i2c_proxy!");
                return;
            }
        };
        let rotation = if CONFIG.display_rotate {
            DisplayRotation::Rotate180
        } else {
            DisplayRotation::Rotate0
        };
        let mut display = Ssd1306::new(I2CDisplayInterface::new(i2c), DisplaySize128x32, rotation)
            .into_buffered_graphics_mode();

        match display.init() {
            Ok(_) => info!("display initialized"),
            Err(e) => error!("display init failed! {:?}", e),
        };
        display.clear_buffer();
        display_dimensions(&mut display);
        display.flush().ok();
        self.display.write().replace(display);
        info!("setup_display end");
    }

    pub(crate) fn display_touch(&self, touched: &[u16], active: &[bool]) {
        if let Err(error) = self.display_action(|display| {
            display.clear_buffer();

            for (pad, &touch) in touched.iter().enumerate() {
                let top = if pad < 5 { -1 } else { 16 };
                let origin = Point::new(4 + 25 * (pad as i32 % 5), top);
                render_text(display, &self.font_tiny, origin, &format!("{}", touch))?;
            }

            for (pad, &is_active) in active.into_iter().enumerate() {
                let top = if pad < 5 { 0 } else { 15 };
                let top_left = Point::new(25 * (pad as i32 % 5), top);
                if is_active {
                    Rectangle::new(top_left, Size::new(26, 17))
                        .into_styled(PrimitiveStyle::with_stroke(BinaryColor::On, 1))
                        .draw(display)
                        .map_err(|e| anyhow!("{e:?}"))?;
                }
            }
            display
                .flush()
                .map_err(|e| anyhow!("display.flush failed: {:?}", e))
        }) {
            error!("display_touch failed: {error}");
        };
    }

    fn flash_toggle(&self) {
        if let Err(e) = self.display_action(|display| {
            let x_offset: u32 = 53;
            for y in 0..31_u32 {
                for x in 0..20_u32 {
                    if (y < 17 && (y + 1) / 2 + x < 7) // triangle on top left not part of arrow
                    || (y < 11 && x+y > 20) // triangle on top right not part of arrow
                    || (y > 10 && x+y > 31) // triangle bottom right not part of arrow
                    || (y > 16 && x+(y-17)/2 < 5)
                    // triangle bottom left not part of arrow
                    {
                        continue;
                    }
                    display.toggle_pixel(x_offset + x, y);
                }
            }
            Ok(())
        }) {
            warn!("display flash toggle failed: {e}");
        }
    }

    fn display_next_history(
        &self,
        variable: &mut MeasurementVariable,
        collisions: &mut Vec<(u32, u32, bool)>,
    ) {
        *variable = MeasurementVariable::progress_variable(variable.to_owned(), 1);
        let history = APP_STATE.bme280_history();
        let mut min = f32::MAX;
        let mut max = f32::MIN;
        for m in history.0.iter().flatten() {
            let value = m.get_variable(variable);
            min = min.min(value);
            max = max.max(value);
        }
        let diff = max - min;

        if let Err(error) = self.display_action(|display| {
            display.clear_buffer();
            render_text(
                display,
                &self.font_small,
                Point::new(0, -1),
                &format!("{}-", four_digits(min)),
            )?;
            render_text(
                display,
                &self.font_small,
                Point::new(0, 16),
                &format!(
                    "{}{}",
                    four_digits(max),
                    MeasurementVariable::get_unit(variable)
                ),
            )?;

            let mut ys: [u32; 128] = [u32::MAX; 128];
            let mut first: bool = true;
            for (xi, m) in history.0[(history.1 + 1)..]
                .iter()
                .chain(history.0[..=history.1].iter())
                .enumerate()
            {
                let x = xi as u32;
                if let Some(m) = m {
                    let value = m.get_variable(variable);
                    ys[xi] = 31 - (31_f32 * ((value - min) / diff)) as u32;
                    // info!("{x},{y}=({value}-{min})/{diff}");
                    if is_pixel_or_neighbor_on(&display, x, ys[xi], 3) {
                        collisions.push((x, ys[xi], false));
                    }
                    first = false;
                } else if !first {
                    // ys[xi] = unsafe{sys::esp_random()} >> 27;
                    collisions.push((x, u32::MAX, true));
                }
            }
            for (x, y) in ys.iter().enumerate() {
                display.toggle_pixel(x as u32, *y);
            }
            Ok(())
        }) {
            error!("display_next_history failed: {error}");
        };
    }

    fn display_timer(&self, start: &i64) {
        if let Err(e) = self.display_action(|display| {
            let passed = unsafe { esp_timer_get_time() } - start;
            let formatted = format!("{}", format_duration(Duration::from_micros(passed as _)));
            display.clear_buffer();
            render_text(
                display,
                &self.font_small,
                Point::new(0, -1),
                &formatted[0..13],
            )?;
            render_text(
                display,
                &self.font_small,
                Point::new(0, 16),
                &formatted[13..],
            )
            .map(|_| ())
        }) {
            error!("display_timer failed: {e}");
        }
    }

    fn display_scroll(
        &self,
        bme280_format: &mut Option<String>,
        display_shift: &mut i32,
        display_speed: i32,
    ) -> bool {
        debug!("if we reach this code, it means DisplayState is not Off, therefore = Scroll");
        if let Some(m) = APP_STATE.bme280_last() {
            bme280_format.replace(m.scroll_text());
        }
        if let Some(format) = &bme280_format
            && let Some(mut guard) = self.display.try_write()
            && let Some(display) = guard.as_mut()
        {
            display.clear_buffer();
            if let Ok(dim) = &self.font_big.render(
                format.as_str(),
                Point::new(*display_shift, 3),
                VerticalPosition::Top,
                FontColor::Transparent(BinaryColor::On),
                display,
            ) {
                *display_shift = (*display_shift - display_speed)
                    % (dim.advance.x - display.dimensions().0 as i32);
                // info!("scroll render dim = {:?}", dim);
            }
            //FreeRtos says 10ms is the smallest that should be used to allow other tasks a chance to run too.
            delay::FreeRtos::delay_ms(10);
            true
        } else {
            debug!("no first successful bme280 measurement yet, give it time to startup");
            delay::FreeRtos::delay_ms(100);
            false
        }
    }
}

fn is_pixel_or_neighbor_on(display: &DisplayDriver, x: u32, y: u32, distance: u32) -> bool {
    for xn in (x - distance)..=(x + distance) {
        for yn in (y - distance)..=(y + distance) {
            if let Some(true) = display.get_pixel(xn, yn) {
                return true;
            }
        }
    }
    false
}

fn four_digits(value: f32) -> String {
    match value as i32 {
        0..=9 => format!("{:.3}", value),
        10..=99 | -9..=-1 => format!("{:.2}", value),
        100..=999 | -99..=-10 => format!("{:.1}", value),
        _ => format!("{}", value as i32),
    }
}

fn render_text(
    display: &mut DisplayDriver,
    font: &FontRenderer,
    origin: Point,
    text: &str,
) -> anyhow::Result<RenderedDimensions> {
    font.render(
        text,
        origin,
        VerticalPosition::Top,
        FontColor::Transparent(BinaryColor::On),
        display,
    )
    .map_err(|e| anyhow!("font.render failed: {e:?}"))
}

pub fn display_fill(display: &mut DisplayDriver) {
    match Rectangle::new(Point::new(0, 0), Size::new(128, 32))
        .into_styled(PrimitiveStyle::with_fill(BinaryColor::On))
        .draw(display)
    {
        Ok(_) => info!("Rectangle drawn"),
        Err(e) => error!("Rectangle draw failed! {:?}", e),
    };
}

pub fn display_dimensions(display: &mut DisplayDriver) {
    match Rectangle::new(Point::new(0, 0), Size::new(128, 32))
        .into_styled(PrimitiveStyle::with_stroke(BinaryColor::On, 1))
        .draw(display)
    {
        Ok(_) => info!("Rectangle drawn"),
        Err(e) => error!("Rectangle draw failed! {:?}", e),
    };
}

pub static DISPLAY_HANDLER: DisplayHandler = DisplayHandler::instance();
