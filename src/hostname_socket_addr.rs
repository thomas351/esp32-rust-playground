use core::fmt;
use esp_idf_svc::hal::delay;
use log::error;
use std::{
    io,
    net::{SocketAddr, TcpStream, ToSocketAddrs},
    num::ParseIntError,
    str::FromStr,
    time::Duration,
};

use crate::config::CONFIG;

#[derive(Debug, Clone)]
pub struct HostnameSocketAddr {
    pub addr: SocketAddr,
}

impl FromStr for HostnameSocketAddr {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (host, port_str) = match s.find(':') {
            Some(i) => (&s[..i], &s[i + 1..]),
            None => return Err("socket address expected in this format: 'host:port'".to_owned()),
        };
        let port: u16 = port_str
            .parse()
            .map_err(|e: ParseIntError| format!("couldn't parse port {port_str}: {}", e))?;
        let mut addr = (host, port).to_socket_addrs().map_err(|e| e.to_string())?;
        let addr = addr
            .next()
            .ok_or_else(|| format!("could not resolve hostname = {host}"))?;
        Ok(HostnameSocketAddr { addr })
    }
}

impl fmt::Display for HostnameSocketAddr {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.addr)
    }
}

impl HostnameSocketAddr {
    pub fn connect(&self) -> TcpStream {
        loop {
            match TcpStream::connect_timeout(
                &self.addr,
                Duration::from_millis(CONFIG.influxdb_connection_timeout_ms as u64),
            ) {
                Ok(stream) => return stream,
                Err(err) => {
                    error!("Failed to connect to addr {}: {err} - pausing a second to let network settle", self.addr);
                    delay::FreeRtos::delay_ms(1000);
                }
            }
        }
    }

    pub fn connect_timeout(&self, timeout: Duration) -> io::Result<TcpStream> {
        TcpStream::connect_timeout(&self.addr, timeout)
    }
}
