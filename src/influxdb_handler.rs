use std::net::Shutdown;

use crossbeam_channel::{Receiver, Sender};
use esp_idf_svc::sys::esp_timer_get_time;
use log::{debug, error, info};

use crate::{
    app_state::APP_STATE, config::DERIVED_CONFIG, hostname_socket_addr::HostnameSocketAddr,
    human_readable_duration::format_duration,
};

pub fn influxdb_sender(
    rx: Receiver<String>,
    target: Option<HostnameSocketAddr>,
) -> anyhow::Result<()> {
    debug!("data_sender started");
    match target {
        None => loop {
            info!("data_sender: no target specified, only printing to stdout instead");
            match rx.recv() {
                Ok(s) => println!("{s}"),
                Err(error) => error!("failed receiving string via mpsc receiver: {error}"),
            }
        },
        Some(target) => {
            debug!("data_sender: using tcp target: {target}");
            let mut stream = target.connect();
            loop {
                match rx.recv() {
                    Ok(mut s) => {
                        log::log!(DERIVED_CONFIG.log_influxdb, "data_sender: sending: {s}");
                        s.push('\n');
                        let mut result = std::io::Write::write(&mut stream, s.as_bytes());
                        while result.is_err() {
                            stream.shutdown(Shutdown::Both).ok(); //don't care if shutdown fails
                            stream = target.connect();
                            result = std::io::Write::write(&mut stream, s.as_bytes());
                        }
                    }
                    Err(error) => {
                        error!("failed receiving string via mpsc receiver: {error}")
                    }
                }
            }
        }
    }
}

pub fn send_uptime(tx: &Sender<String>) {
    log::log!(
        DERIVED_CONFIG.log_uptime,
        "uptime = {}",
        format_duration(APP_STATE.timer_service.now())
    );
    tx.try_send(format!(
        ",mac={} esp32.uptime={}",
        APP_STATE.mac(),
        unsafe { esp_timer_get_time() }
    ))
    .ok();
}
