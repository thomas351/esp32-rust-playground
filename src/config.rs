use std::{borrow::Cow, collections::HashMap, str::FromStr};

use log::{error, Level, LevelFilter};
use once_cell::sync::Lazy;

#[cfg(feature = "display")]
use crate::display_handler::DisplayStateChange;

/// This configuration is picked up at compile time by `build.rs` from the file `cfg.toml`.
#[derive(Debug)]
#[toml_cfg::toml_config]
pub struct Config {
    #[default("specify_in_cfg.toml")]
    wifi_ssid: &'static str,
    #[default("specify_in_cfg.toml")]
    wifi_psk: &'static str,
    #[default("specify_in_cfg.toml")]
    wifi_ap_pwd: &'static str,
    #[default("Debug")]
    log_wifi_info: &'static str,
    #[default("Debug")]
    log_uptime: &'static str,
    #[default("Debug")]
    log_influxdb: &'static str,
    #[default("Debug")]
    log_bme280: &'static str,
    #[default("Debug")]
    log_bme280_dropped: &'static str,
    #[default("Debug")]
    log_thread_status: &'static str,
    #[default("Info")]
    log_max_level: &'static str,
    #[default("Warn")]
    log_buffer_filter: &'static str,
    #[default(1000*60)]
    thread_status_frequency_ms: u32,
    #[default(500)]
    influxdb_connection_timeout_ms: u32,
    #[default("VictoriaMetricsHost:8089")]
    tcp_target: &'static str,
    #[default(60*5)]
    min_uptime_before_reboot_sec: i64,
    #[default(60*3)]
    bme280_startup_delay_on_unexpected_reset_reason_sec: u32,
    #[default(3000)]
    bme280_period_ms: u32,
    #[default(5)]
    bme280_allowed_skips: u32,
    #[default(5)]
    bme280_check_every_secs: u64,
    #[default(100)]
    bme280_retry_ms: u32,
    #[default(10)]
    wifi_check_every_secs: u64,
    #[default(60)]
    clear_display_after_secs: u64,
    #[default(0.004)]
    expected_temperature_step: f32,
    #[default(0.004)]
    expected_humidity_step: f32,
    #[default(0.4)]
    expected_pressure_step: f32,
    #[default(1_111_111_111)]
    uptime_check_every_nanos: u64,
    #[default("syslog-server-host:514")]
    syslog_target: &'static str,
    #[default("/home/user/.cargo/registry/src/index.crates.io-6f17d22bba15001f/")]
    log_strip_prefix: &'static str,
    #[default(16384)]
    log_buffer_size: usize,
    #[default(4096)]
    warn_buffer_size: usize,
    #[default(4096)]
    stack_size_tcp_sender: usize,
    #[default(8192)]
    stack_size_display: usize,
    #[default(4096)]
    stack_size_logger: usize,
    #[default(8192)]
    stack_size_i2c: usize,
    #[default(4096)]
    stack_size_influxdb_sender: usize,
    #[default(false)]
    display_rotate: bool,
    #[default(40)]
    button_threshold_active_default: u16,
    #[default(60)]
    button_threshold_inactive_default: u16,
    #[default("")]
    button_threshold_active_custom: &'static str,
    #[default("")]
    button_threshold_inactive_custom: &'static str,
    #[default("0:RelayToggle,7:Toggle,8:Minus,9:Plus,1:TimerToggle")]
    button_functions: &'static str,
}

pub struct DerivedConfig {
    pub log_wifi_info: Level,
    pub log_uptime: Level,
    pub log_influxdb: Level,
    pub log_bme280: Level,
    pub log_bme280_dropped: Level,
    pub log_thread_status: Level,
    pub log_max_level: Level,
    pub log_max_level_filter: LevelFilter,
    pub button_threshold_active_custom: HashMap<usize, u16>,
    pub button_threshold_inactive_custom: HashMap<usize, u16>,
    #[cfg(feature = "display")]
    pub button_functions: HashMap<usize, DisplayStateChange>,
}
pub static DERIVED_CONFIG: Lazy<DerivedConfig> = Lazy::new(|| DerivedConfig {
    log_wifi_info: log::Level::from_str(CONFIG.log_wifi_info).unwrap_or(Level::Trace), //.unwrap_or_else(Level::Trace),
    log_uptime: log::Level::from_str(CONFIG.log_uptime).unwrap_or(Level::Trace),
    log_influxdb: log::Level::from_str(CONFIG.log_influxdb).unwrap_or(Level::Trace),
    log_bme280: log::Level::from_str(CONFIG.log_bme280).unwrap_or(Level::Trace),
    log_bme280_dropped: log::Level::from_str(CONFIG.log_bme280_dropped).unwrap_or(Level::Trace),
    log_thread_status: log::Level::from_str(CONFIG.log_thread_status).unwrap_or(Level::Trace),
    log_max_level: log::Level::from_str(CONFIG.log_max_level).unwrap_or(Level::Trace),
    log_max_level_filter: LevelFilter::from_str(CONFIG.log_max_level).unwrap_or(LevelFilter::Trace),
    button_threshold_active_custom: parse_map(CONFIG.button_threshold_active_custom),
    button_threshold_inactive_custom: parse_map(CONFIG.button_threshold_inactive_custom),
    #[cfg(feature = "display")]
    button_functions: parse_map(CONFIG.button_functions),
});

fn parse_map<K, V>(input: &str) -> HashMap<K, V>
where
    K: FromStr + Eq + std::hash::Hash,
    V: FromStr,
{
    let mut map = HashMap::new();
    for pair in input.split(',') {
        if let [key, value] = pair.split(':').collect::<Vec<&str>>()[..]
        && let Ok(parsed_key) = key.parse::<K>()
        && let Ok(parsed_value) = value.parse::<V>() {
           map.insert(parsed_key, parsed_value);
       }
       else {
           error!("Config parse_map failed on pair: '{pair}'");
       }
    }
    map
}

pub fn escaped_value(src: &'static str) -> Cow<'static, str> {
    if src.contains("\"") {
        Cow::Owned(src.replace("\"", "\\\""))
    } else {
        Cow::Borrowed(src)
    }
}
