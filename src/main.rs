// combine multiple if let constructs with each other and/or boolean conditions
#![feature(let_chains)]
// needed for printf_compat library
#![feature(c_variadic)]
#![feature(fn_traits)]
//using .each_ref() in app_state function touched ~line 175
//#![feature(array_methods)]
// the feature `array_methods` has been stable since 1.77.0 and no longer requires an attribute to enable

// #![feature(const_trait_impl)]

// thought about using this for taking multiple bytes at once from the log buffers but ended up not doing it like that
// #![feature(iter_next_chunk)]

mod app_state;
mod bme280_handler;
mod buffered_logger;
mod config;
mod hostname_socket_addr;
mod human_readable_duration;
mod influxdb_handler;
mod log_buffer;
mod wifi_handler;

#[macro_use]
extern crate cfg_if;
cfg_if! {if #[cfg(feature = "display")] {
    mod display_handler;
    use crate::display_handler::{DisplayStateChange, DISPLAY_HANDLER};
    use strum::IntoEnumIterator;
}}

use crate::{
    app_state::{start_thread, take_join_handle, Bme280RelayRule},
    bme280_handler::{bme280_freeze_detector, bme280_loop, setup_bme280, MeasurementVariable},
    buffered_logger::{esp_log_processor, log_compile_info, log_config},
    config::{CONFIG, DERIVED_CONFIG},
    hostname_socket_addr::HostnameSocketAddr,
    influxdb_handler::{influxdb_sender, send_uptime},
    log_buffer::LOG_BUFFER,
    wifi_handler::{check_wifi, wifi_setup},
};
use app_state::APP_STATE;
use bme280_handler::Measurement;
use buffered_logger::BUFFERED_LOGGER;
use crossbeam_channel::{bounded, Sender};
use embedded_svc::{
    http::{server::Request, Headers, Method},
    io::{self, Write},
};

use esp_idf_svc::{
    hal::{
        delay,
        gpio::{Gpio19, Gpio21, Gpio22},
        i2c::I2C0,
        prelude::Peripherals,
        reset::ResetReason,
    },
    http::server::{EspHttpConnection, EspHttpServer},
    log::EspLogger,
    netif::IpEvent,
    ota::EspOta,
    sys::{
        //self as _, // If using the `binstart` feature of `esp-idf-sys`, always keep this module imported
        self as sys, // If using the `binstart` feature of `esp-idf-sys`, always keep this module imported
        esp_get_free_heap_size,
        esp_get_free_internal_heap_size,
        esp_get_minimum_free_heap_size,
        esp_log_set_vprintf,
        esp_restart,
        esp_timer_get_time,
        link_patches,
    },
    wifi::{EspWifi, WifiEvent},
};

use human_readable_duration::format_duration;
use lenient_bool::LenientBool;
use log::{error, info, log, warn, Level, LevelFilter};
#[cfg(feature = "touch")]
use std::ffi::CStr;
use std::{borrow::Cow, cmp::min, str::FromStr, sync::OnceLock, thread, time::Duration};

#[allow(dead_code)]
#[no_mangle]
pub extern "C" fn symlink(_old: *const u8, _new: *const u8) {
    unimplemented!()
}

static ESP_LOGGER: OnceLock<EspLogger> = OnceLock::new();

fn main() -> anyhow::Result<()> {
    println!("ESP32 Rust Main started");

    // It is necessary to call this function once. Otherwise some patches to the runtime
    // implemented by esp-idf-sys might not link properly. See https://github.com/esp-rs/esp-idf-template/issues/71
    link_patches();
    println!("esp_idf_sys::link_patches() ran");

    let peripherals = Peripherals::take().unwrap();
    println!("Peripherals::take().unwrap() ran");

    wifi_setup(peripherals.modem)?;
    println!("wifi initialized");

    // for now all logging before wifi initialization can only go to the serial terminal via println macro,
    // because our syslog setup requires socket creation, which causes boot-loop panic if done before wifi init:
    // assert failed: tcpip_send_msg_wait_sem IDF/components/lwip/lwip/src/api/tcpip.c:455 (Invalid mbox)
    // see also: https://github.com/espressif/esp-idf/issues/9782
    LOG_BUFFER.write().syslog_init();
    if let Err(error) = ::log::set_logger(&BUFFERED_LOGGER) {
        eprintln!("log::set_logger(buffered_logger) failed! {error}");
        let esp_logger = ESP_LOGGER.get_or_init(|| EspLogger::default());
        if let Err(error) = ::log::set_logger(esp_logger) {
            eprintln!("log::set_logger(ESP_LOGGER) (=fallback) failed too! {error}");
        }
        esp_logger.initialize();
        if let Err(error) = esp_logger.set_target_level("*", DERIVED_CONFIG.log_max_level_filter) {
            eprintln!("ESP_LOGGER.set_target_level(*) failed! {error}");
        }
        if let Err(error) = esp_logger.set_target_level("rust-logging", LevelFilter::Off) {
            eprintln!("ESP_LOGGER.set_target_level(rust-logging) failed! {error}");
        }
    }
    ::log::set_max_level(DERIVED_CONFIG.log_max_level_filter);
    log!(
        min(DERIVED_CONFIG.log_max_level, Level::Info),
        "logger initialized"
    );
    unsafe {
        if let Some(default_esp_logger) = esp_log_set_vprintf(Some(esp_log_processor)) {
            APP_STATE.default_esp_logger.set(default_esp_logger).ok();
        }
    }

    log_compile_info();
    log_config();

    let reset_reason = ResetReason::get();
    warn!("ResetReason was: {:?}", reset_reason);
    // Observed Reasons:
    // first boot after initial flash via ota_setup.sh = PowerOn
    // reboot caused by failed I2C setup using forbidden pin6 = InterruptWatchdog
    // using ota_update.sh = Software
    // tio /dev/ttyUSB0 ; ctrl+t g 0 ; ctrl+t g 1 = PowerOn
    // cargo espmonitor /dev/ttyUSB0 ; ctrl+r = PowerOn
    // BrownOut

    let (tx, rx) = bounded(64);

    // let wifi_clone = wifi.clone();
    // let mac_clone = mac.clone();
    let tx_clone = tx.clone();
    let _wifi_watcher = APP_STATE.sysloop.subscribe::<WifiEvent, _>(move |event| {
        warn!("Wifi Event happened: {:?}", event);
        check_wifi(&tx_clone, true);
    })?;

    let tx_clone = tx.clone();
    let _ip_watcher = APP_STATE.sysloop.subscribe::<IpEvent, _>(move |event| {
        warn!("IpEvent happened: {:?}", event);
        check_wifi(&tx_clone, true);
    })?;

    info!("starting webserver now");
    let _webserver = webserver_setup()?;

    info!("spawning data_sender thread");
    start_thread(
        "data_sender\0",
        2,
        CONFIG.stack_size_influxdb_sender,
        move || {
            influxdb_sender(
                rx,
                match HostnameSocketAddr::from_str(CONFIG.tcp_target) {
                    Ok(h) => Some(h),
                    Err(err) => {
                        error!(
                            "couldn't parse or resolve tcp_target: '{}' - error: {err}",
                            CONFIG.tcp_target
                        );
                        None
                    }
                },
            )
            .map(|_| error!("data_sender has exited without error!"))
            .or_else(|error| {
                Ok::<(), anyhow::Error>(error!("data_sender has exited with error: {error}"))
            })
            .ok();
        },
    );

    tx.try_send(format!(
        ",mac={},resetReason={:?} esp32.uptime=0",
        APP_STATE.mac(),
        reset_reason
    ))
    .ok();

    let tx_clone = tx.clone();
    APP_STATE.schedule(
        Duration::from_nanos(CONFIG.uptime_check_every_nanos),
        true,
        move || send_uptime(&tx_clone),
    );

    let tx_clone = tx.clone();
    APP_STATE.schedule(
        Duration::from_secs(CONFIG.wifi_check_every_secs),
        true,
        move || check_wifi(&tx_clone, false),
    );

    info!("spawning i2c thread: not implemented as timer, so that when an i2c call freezes a thread it's not the shared timer thread");
    start_thread("i2c\0", 3, CONFIG.stack_size_i2c, move || {
        i2c_setup_and_loop(
            peripherals.i2c0,
            peripherals.pins.gpio21,
            peripherals.pins.gpio22,
            reset_reason,
            tx,
            peripherals.pins.gpio19,
        );
    });

    if DERIVED_CONFIG.log_thread_status <= DERIVED_CONFIG.log_max_level_filter {
        warn!("Setup done! Will check & log thread + timer status frequently, because CONFIG.log_thread_status <= CONFIG.log_max_level ({}<={})",
            DERIVED_CONFIG.log_thread_status,
            DERIVED_CONFIG.log_max_level_filter
        );
        loop {
            match APP_STATE.timers.try_lock() {
                Ok(timers) => {
                    for (i, timer) in timers.iter().enumerate() {
                        log::log!(
                            DERIVED_CONFIG.log_thread_status,
                            "timer #{i} scheduled: {:?}",
                            timer.is_scheduled()
                        );
                    }
                }
                Err(error) => error!("APP_STATE.timers.try_lock() error: {:?}", error),
            }
            match APP_STATE.threads.read().as_ref() {
                Some(threads) => {
                    for (name, thread) in threads.iter() {
                        match thread {
                            Some(thread) => {
                                log::log!(
                                    DERIVED_CONFIG.log_thread_status,
                                    "thread {name}: {:?}; finished = {}",
                                    thread.thread(),
                                    thread.is_finished()
                                );
                            }
                            None => error!("handle of thread {name} is None! This should only happen for threads that have been taken for joining!"),
                        }
                    }
                }
                None => error!("no threads have been started! this can't be right!"),
            }
            delay::FreeRtos::delay_ms(CONFIG.thread_status_frequency_ms);
        }
    } else {
        info!("Setup done, waiting on spawned threads to prevent cleanup of our setup (which causes wifi disconnect, webserver stop and so on..)");
        while let (name, Some(handle)) = take_join_handle() {
            match handle.join() {
                Ok(_) => warn!("thread {name} has ended!"),
                Err(e) => error!("thread {name} failed: {:?}", e),
            }
        }
        error!("All threads have ended, even though most of them never should! Setup will be undone and the system will be even worse off then it already was :(");
        Ok(())
    }
}

fn i2c_setup_and_loop(
    i2c: I2C0,
    sda: Gpio21,
    scl: Gpio22,
    reset_reason: ResetReason,
    tx: Sender<String>,
    relay_pin: Gpio19,
) {
    match reset_reason {
        ResetReason::Software // happens on OTA update / http reboot command / bme280 freeze detection
        | ResetReason::PowerOn // cold-start, power supply reconnected after being without power - or also first boot after complete re-flash (e.g. via ota_setup.sh)
        | ResetReason::ExternalPin // don't know, but sounds harmless..
        // | ResetReason::Brownout // I believe this signifies problems with too little power, therefore I'm removing this from the acceptable reboot_reasons list, because activating i2c devices would very likely increase power usage
        // | ResetReason::InterruptWatchdog // reboot caused by failed I2C setup using forbidden pin6
         => {
            info!("reset_reason is on the acceptable reasons list, setting up i2c devices immediately. reset reason = {:?}", reset_reason);
        }
        _ => {
            error!(
                "delaying I2C setup for {} seconds, so that you have a chance to use OTA_update if unexpected reset reason was caused by bad i2c code: {:?}",
                CONFIG.bme280_startup_delay_on_unexpected_reset_reason_sec, reset_reason
            );
            delay::FreeRtos::delay_ms(
                CONFIG.bme280_startup_delay_on_unexpected_reset_reason_sec * 1000,
            );
        }
    };

    *APP_STATE.esp_log_vararg_parsing.write() = true;
    APP_STATE.init_i2c_bus(i2c, sda, scl);
    setup_bme280();

    let tx_clone = tx.clone();
    APP_STATE.schedule(
        Duration::from_secs(CONFIG.bme280_check_every_secs),
        true,
        move || bme280_freeze_detector(&tx_clone),
    );

    #[cfg(feature = "display")]
    DISPLAY_HANDLER.setup_display_thread().ok();

    #[cfg(feature = "touch")]
    touch_setup();

    if let Err(error) = APP_STATE.relay_init(relay_pin) {
        error!("relay_init failed! - {error}");
    }

    bme280_loop(tx);
}

#[cfg(feature = "touch")]
unsafe fn esp_err_str<'a>(result: sys::esp_err_t) -> Cow<'a, str> {
    CStr::from_ptr(sys::esp_err_to_name(result)).to_string_lossy()
}

#[cfg(feature = "touch")]
fn touch_setup() {
    info!("starting touch setup");

    info!("touch_pad_init: {}", unsafe {
        esp_err_str(sys::touch_pad_init())
    });

    let mut sleep_cycle = 0u16;
    let mut meas_cycle: u16 = 0u16;
    info!(
        "touch_pad_get_meas_time: {} -> {}, {}",
        unsafe {
            esp_err_str(sys::touch_pad_get_meas_time(
                &mut sleep_cycle,
                &mut meas_cycle,
            ))
        },
        sleep_cycle,
        meas_cycle
    );
    // defaults given: sleep = 4096, meas = 32767

    info!("touch_pad_set_meas_time: {}", unsafe {
        esp_err_str(sys::touch_pad_set_meas_time(12000_u16, 4000_u16))
    });

    for i in sys::touch_pad_t_TOUCH_PAD_NUM0..sys::touch_pad_t_TOUCH_PAD_MAX
    // .filter(|&x| x != sys::touch_pad_t_TOUCH_PAD_NUM1)
    {
        // let thresh = if i != sys::touch_pad_t_TOUCH_PAD_NUM1 {
        //     0
        // } else {
        //     200
        // };
        info!("touch_pad_config({i}): {}", unsafe {
            esp_err_str(sys::touch_pad_config(i, 150))
        });
        // info!("touch_pad_set_thresh({i}): {}", unsafe {
        //     esp_err_str(sys::touch_pad_set_thresh(i, thresh))
        // });
    }

    // info!("touch_pad_config(0,150): {}", unsafe {
    //     esp_err_str(sys::touch_pad_config(0, 150))
    // });

    //skip pad1 since it's pulled down by the serial adapter we use in our debug setup, because it's reused as DTR = IO0 pin.
    //actually skip 1 & 8 since I don't know which bit is what
    // let active: u16 = 0b0000_0010_1111_1101;
    // or instead lets just try first and last
    // let active: u16 = 0b0000_0010_0000_0001;
    // info!("touch_pad_set_group_mask: {}", unsafe {
    //     esp_err_str(sys::touch_pad_set_group_mask(active, active, active))
    // });
    // info!("touch_pad_set_trigger_mode: {}", unsafe {
    //     esp_err_str(sys::touch_pad_set_trigger_mode(
    //         sys::touch_trigger_mode_t_TOUCH_TRIGGER_BELOW,
    //     ))
    // });
    // info!("touch_pad_set_trigger_source: {}", unsafe {
    //     esp_err_str(sys::touch_pad_set_trigger_source(
    //         sys::touch_trigger_src_t_TOUCH_TRIGGER_SOURCE_SET1,
    //     ))
    // });

    info!("touch_pad_set_filter_read_cb : {}", unsafe {
        esp_err_str(sys::touch_pad_set_filter_read_cb(Some(touch_happened)))
    });
    info!("touch_pad_filter_start: {}", unsafe {
        esp_err_str(sys::touch_pad_filter_start(20u32))
    });

    // let zero_and_nine: u16 = 0b0000_0010_0000_0001;
    // let none: u16 = 0b0000_0000_0000_0000;
}

/*
Param raw_value
    The latest raw data(touch sensor counter value) that points to all channels(raw_value[0..TOUCH_PAD_MAX-1]).

Param filtered_value
    The latest IIR filtered data(calculated from raw data) that points to all channels(filtered_value[0..TOUCH_PAD_MAX-1]).
 */
/// # Safety
///
/// This callback needs to be unsafe because it's called from esp-idf C-code
#[cfg(feature = "touch")]
pub unsafe extern "C" fn touch_happened(_raw_value: *mut u16, filtered_value: *mut u16) {
    APP_STATE.touched(std::slice::from_raw_parts(
        filtered_value,
        sys::touch_pad_t_TOUCH_PAD_MAX as _,
    ));

    // let raw_slice = std::slice::from_raw_parts(raw_value, sys::touch_pad_t_TOUCH_PAD_MAX as _);
    // let filtered_slice =
    //     std::slice::from_raw_parts(filtered_value, sys::touch_pad_t_TOUCH_PAD_MAX as _);

    // let mut touched = [false; 10];
    // for i in (sys::touch_pad_t_TOUCH_PAD_NUM0..sys::touch_pad_t_TOUCH_PAD_MAX).map(|a| a as usize) {
    //     let filtered = filtered_slice[i];
    //     if filtered == 0 {
    //         debug!("touch pad, filtered[{i}]=0 -> invalid value, won't consider it!");
    //         continue;
    //     }
    //     if touched[i] && filtered > 300 {
    //         info!("touch pad {i} was active, deactivated with value {filtered}");
    //         touched[i] = false;
    //     }
    //     touched[i] = filtered > 0 && ((touched[i] && filtered < 300) || filtered < 150);
    // }

    // let mut non_zero_found = false;
    // let mut values = String::with_capacity((sys::touch_pad_t_TOUCH_PAD_MAX * (3 + 1 + 3 * 2)) as _);
    // // let mut bool:
    // for i in sys::touch_pad_t_TOUCH_PAD_NUM0..sys::touch_pad_t_TOUCH_PAD_MAX {
    //     let raw = raw_slice[i as usize];
    //     let filtered = filtered_slice[i as usize];

    //     // if raw > 0 || filtered > 0 {
    //     //     non_zero_found = true;
    //     // }
    //     values += &format!("{i}={raw}/{filtered};"); // length: 3 separator chars, i = 0-9 = 1 char, raw & filtered probably 3 chars each: 3+1+3*2
    // }
    // info!("touch_happened: {values}");
    // if !non_zero_found {
    //     debug!("all zeroes touch event happened..");
    // }
}

fn web_get<'b, E, F>(
    server: &'b mut EspHttpServer<'static>,
    gets: &mut Vec<Cow<str>>,
    uri: Cow<'static, str>,
    f: F,
) -> Result<&'b mut EspHttpServer<'static>, anyhow::Error>
where
    F: for<'r> Fn(Request<&mut EspHttpConnection<'r>>) -> Result<(), E> + Send + 'static,
    E: core::fmt::Debug,
{
    let result = server
        .fn_handler(uri.as_ref(), Method::Get, f)
        .map_err(|e| anyhow::anyhow!(e));
    gets.push(uri);
    result
}

fn webserver_setup() -> anyhow::Result<EspHttpServer<'static>> {
    let mut server = EspHttpServer::new(&Default::default())?;
    let mut gets: Vec<Cow<str>> = Vec::new();
    web_get(
        &mut server,
        &mut gets,
        "/uptime".into(),
        move |req| -> Result<(), _> {
            let uptime = format_duration(APP_STATE.timer_service.now());
            info!("http uptime queried: {uptime}");
            io::Write::write_all(
                &mut req.into_response(200, Some("OK"), &[("Content-Type", "text/plain")])?,
                format!("{uptime}\n").as_bytes(),
            )?;
            Ok::<(), anyhow::Error>(())
        },
    )?;
    web_get(&mut server, &mut gets, "/heap".into(), |req| {
        let mut resp = req.into_response(200, Some("OK"), &[("Content-Type", "text/plain")])?;
        let heap_free = unsafe { esp_get_free_heap_size() };
        let heap_internal = unsafe { esp_get_free_internal_heap_size() };
        let heap_min = unsafe { esp_get_minimum_free_heap_size() };
        let format = format!(
            "heap free: {}\r\nheap internal: {}\r\nheap minimum: {}",
            heap_free, heap_internal, heap_min
        );
        info!("{format}");
        io::Write::write_all(&mut resp, format.as_bytes())?;
        io::Write::write_all(&mut resp, &[b'\n'])?;
        Ok::<(), anyhow::Error>(())
    })?;
    web_get(&mut server, &mut gets, "/log".into(), |req| {
        let mut resp = req.into_response(200, Some("OK"), &[("Content-Type", "text/plain")])?;
        let mut i = 0;
        let mut write_buffer = [0u8; 256];
        // since writing each byte to the network on it's own is very slow, we'll send 256 bytes at once
        for byte in LOG_BUFFER.read().read_all() {
            write_buffer[i] = *byte;
            i += 1;
            if i == write_buffer.len() {
                io::Write::write_all(&mut resp, &write_buffer)?;
                i = 0;
            }
        }
        // in the end, write out what's left plus a line break:
        io::Write::write_all(&mut resp, &write_buffer[..i])?;
        io::Write::write_all(&mut resp, &[b'\n'])?;
        Ok::<(), anyhow::Error>(())
    })?;
    web_get(&mut server, &mut gets, "/problems".into(), |req| {
        let mut resp = req.into_response(200, Some("OK"), &[("Content-Type", "text/plain")])?;
        let mut i = 0;
        let mut write_buffer = [0u8; 256];
        // since writing each byte to the network on it's own is very slow, we'll send 256 bytes at once
        for byte in LOG_BUFFER.read().read_problems() {
            write_buffer[i] = *byte;
            i += 1;
            if i == write_buffer.len() {
                io::Write::write_all(&mut resp, &write_buffer)?;
                i = 0;
            }
        }
        // in the end, write out what's left plus a line break:
        io::Write::write_all(&mut resp, &write_buffer[..i])?;
        io::Write::write_all(&mut resp, &[b'\n'])?;
        Ok::<(), anyhow::Error>(())
    })?;
    web_get(&mut server, &mut gets, "/wifi_scan".into(), |req| {
        let mut resp = req.into_response(200, Some("OK"), &[("Content-Type", "text/plain")])?;
        let mut wifi_guard: std::sync::MutexGuard<'_, once_cell::sync::OnceCell<EspWifi<'static>>> =
            APP_STATE
                .wifi
                .try_lock()
                .map_err(|error| {
                    let result = format!("couldn't acquire APP_STATE.wifi lock! error={error}");
                    error!("http/wifi_scan: {result}");
                    io::Write::write_all(&mut resp, result.as_bytes())
                })
                .map_err(|e| anyhow::anyhow!("{e:?}"))?;
        let wifi: &mut EspWifi<'static> = wifi_guard
            .get_mut()
            .ok_or(())
            .map_err(|_| {
                let error = "wifi setup failed! APP_STATE.wifi is None!";
                error!("http/wifi_scan: {error}");
                io::Write::write_all(&mut resp, error.as_bytes())
            })
            .map_err(|e| anyhow::anyhow!("{e:?}"))?;
        match wifi.scan() {
            Ok(aps) => {
                info!("http/wifi_scan succeeded, results follow:");
                for ap in aps {
                    let result = format!(
                        "ssid={} @channel={} signal={}",
                        ap.ssid, ap.channel, ap.signal_strength
                    );
                    io::Write::write_all(&mut resp, result.as_bytes())?;
                    io::Write::write_all(&mut resp, &[b'\n'])?;
                    info!("{result}");
                }
            }
            Err(error) => {
                let result = format!("wifi scan failed: {error}");
                error!("http/wifi_scan: {result}");
                io::Write::write_all(&mut resp, result.as_bytes())?;
                io::Write::write_all(&mut resp, &[b'\n'])?;
            }
        };
        Ok::<(), anyhow::Error>(())
    })?;
    web_get(&mut server, &mut gets, "/bme280".into(), |req| {
        let mut resp = req.into_response(200, Some("OK"), &[("Content-Type", "text/plain")])?;
        let val: Cow<'static, str> = match (
            APP_STATE.bme280_last_block(),
            APP_STATE.bme280_time.read().as_ref(),
        ) {
            (Some(bme280), Some(time)) => format!(
                "{}, measured {} ago",
                bme280.details(),
                format_duration(Duration::from_micros(
                    (unsafe { esp_timer_get_time() } - time) as _
                ))
            )
            .into(),
            _ => "No successful measurement yet!".into(),
        };
        info!("{val}");
        io::Write::write_all(&mut resp, val.as_bytes())?;
        io::Write::write_all(&mut resp, &[b'\n'])?;
        Ok::<(), anyhow::Error>(())
    })?;
    web_get(&mut server, &mut gets, "/relay_toggle".into(), |req| {
        let mut resp = req.into_response(200, Some("OK"), &[("Content-Type", "text/plain")])?;
        let val = match APP_STATE.relay_toggle(None) {
            Ok(state) => format!("relay state was {}, toggled to {}", !state, state),
            Err(error) => format!("relay toggle failed! {error}"),
        };
        info!("{val}");
        io::Write::write_all(&mut resp, val.as_bytes())?;
        io::Write::write_all(&mut resp, &[b'\n'])?;
        Ok::<(), anyhow::Error>(())
    })?;
    web_get(&mut server, &mut gets, "/relay_state".into(), |req| {
        let mut resp = req.into_response(200, Some("OK"), &[("Content-Type", "text/plain")])?;
        let val = format!("{}", APP_STATE.relay_active.read());
        info!("relay state = {val}");
        io::Write::write_all(&mut resp, val.as_bytes())?;
        io::Write::write_all(&mut resp, &[b'\n'])?;
        Ok::<(), anyhow::Error>(())
    })?;
    web_get(
        &mut server,
        &mut gets,
        "/enable_vararg_parsing".into(),
        |req| {
            let mut resp = req.into_response(200, Some("OK"), &[("Content-Type", "text/plain")])?;
            *APP_STATE.esp_log_vararg_parsing.write() = true;
            let msg = "APP_STATE.esp_log_vararg_parsing set to true.";
            info!("{msg}");
            io::Write::write_all(&mut resp, msg.as_bytes())?;
            io::Write::write_all(&mut resp, &[b'\n'])?;
            Ok::<(), anyhow::Error>(())
        },
    )?;

    #[cfg(feature = "display")]
    for display_state_change in DisplayStateChange::iter() {
        web_get(
            &mut server,
            &mut gets,
            format!("/display_{display_state_change}").into(),
            move |req| {
                if let Some(selector) = DISPLAY_HANDLER.mode_selector.get() {
                    match selector.send(display_state_change) {
                        Ok(()) => io::Write::write_all(
                            &mut req.into_ok_response()?,
                            format!("/display state change requested: {display_state_change}\n").as_bytes(),
                        )?,
                        Err(error) => io::Write::write_all(
                            &mut req.into_ok_response()?,
                            format!(
                                "/failed to request display state change: {display_state_change}, error = {error}\n"
                            )
                            .as_bytes(),
                        )?,
                    }
                } else {
                    io::Write::write_all(
                        &mut req.into_ok_response()?,
                        b"display not initialized yet, can't request display state change\n",
                    )?;
                }
                Ok::<(), anyhow::Error>(())
            },
        )?;
    }
    web_get(&mut server, &mut gets, "/reboot".into(), |req| {
        error!(
            "http client requested reboot! spawning thread that waits a second and then reboots!"
        );
        io::Write::write_all(
            &mut req.into_ok_response()?,
            "Rebooting ESP32 in 1 second!\n".as_bytes(),
        )?;
        APP_STATE.schedule(Duration::from_secs(1), false, || {
            warn!("waited for a second, rebooting now!");
            unsafe { esp_restart() };
        });
        Ok::<(), anyhow::Error>(())
    })?;

    web_get(&mut server, &mut gets, "/touch".into(), |req| {
        let mut resp = req.into_response(200, Some("OK"), &[("Content-Type", "text/plain")])?;
        for i in sys::touch_pad_t_TOUCH_PAD_NUM0..sys::touch_pad_t_TOUCH_PAD_MAX {
            let mut value = 1u16;
            let mut raw = 1u16;
            let mut filtered = 1u16;
            unsafe {
                sys::touch_pad_read(i, &mut value);
                sys::touch_pad_read_raw_data(i, &mut raw);
                sys::touch_pad_read_filtered(i, &mut filtered);
            }
            resp.write_all(format!("#{i}={value}/{raw}/{filtered}\n").as_bytes())?;
        }
        Ok::<(), anyhow::Error>(())
    })?;

    web_get(
        &mut server,
        &mut gets,
        "/bme280_relay_rules_list".into(),
        |req| {
            let mut resp = req.into_response(200, Some("OK"), &[("Content-Type", "text/plain")])?;
            for rule in APP_STATE.bme280_relay_rules.read().iter() {
                resp.write_all(format!("{rule:?}\n").as_bytes())?;
            }
            Ok::<(), anyhow::Error>(())
        },
    )?;

    web_get(
        &mut server,
        &mut gets,
        "/bme280_relay_rules_clear".into(),
        |req| {
            let mut resp = req.into_response(200, Some("OK"), &[("Content-Type", "text/plain")])?;
            APP_STATE.bme280_relay_rules.write().clear();
            resp.write_all("bme280_relay_rules cleared".as_bytes())?;
            Ok::<(), anyhow::Error>(())
        },
    )?;

    server.fn_handler("/bme280_relay_rule_add", Method::Post, |mut request| {
        let length = request.content_len().unwrap_or(0) as usize;
        let mut buffer = vec![0; length];
        request.read(&mut buffer)?;
        let mut response =
            request.into_response(200, Some("OK"), &[("Content-Type", "text/plain")])?;
        let string = String::from_utf8_lossy(&buffer);
        let data: Vec<&str> = string.split(";").collect();
        let rule = Bme280RelayRule {
            variable: MeasurementVariable::from_str(data[0])?,
            value: data[1].parse()?,
            bigger: *data[2]
                .parse::<LenientBool>()
                .map_err(|e| anyhow::anyhow!("{e:?}: Can't parse {} as bool!", data[2]))?,
            relay_target: *data[3]
                .parse::<LenientBool>()
                .map_err(|e| anyhow::anyhow!("{e:?}: Can't parse {} as bool!", data[3]))?,
        };
        let result = format!("bme280_relay_rule post received: {data:?} -- parsed: {rule:?}");
        APP_STATE.bme280_relay_rules.write().push(rule);
        response.write_all(result.as_bytes())?;
        info!("{}", result);
        Ok::<(), anyhow::Error>(())
    })?;

    server.fn_handler("/", Method::Get, move |req| {
        info!("http get request @ /");
        let mut resp = req.into_ok_response()?;
        for get in gets.iter() {
            resp.write_all(format!("<a href=\"{get}\">{get}</a><br />\n").as_bytes())?;
        }
        Ok::<(), anyhow::Error>(())
    })?;

    server.fn_handler("/ota", Method::Put, |req| {
        warn!("received put to /ota:");
        Ok::<(), anyhow::Error>(do_ota(req).map_err(|e| {
            APP_STATE.ota_canceled();
            error!("ota canceled: {e}");
            e
        })?)
    })?;
    Ok(server)
}

fn do_ota(mut req: Request<&mut EspHttpConnection>) -> anyhow::Result<()> {
    APP_STATE.ota_started();
    let (_, connection) = req.split();

    let mut ota = EspOta::new().expect("EspOta::new should have been successful");
    let mut ota_update = ota.initiate_update()?;

    let mut buffer: [u8; 1024] = [0; 1024];
    let mut error_count = 0;
    while error_count < 10 {
        match connection.read(&mut buffer)? {
            0 => {
                error_count += 1;
                warn!("OTA_update: read zero bytes, counting as an error. count={error_count}");
            }
            read => {
                ota_update.write(&buffer[0..read])?;
                // this produces so much spam, that OTA doesn't work anymore because the chip panics before finishing loading it!
                // trace!("OTA_update: processed {read} bytes, continue loop");
            }
        }
    }

    io::Write::write_all(
        &mut req.into_ok_response()?,
        "OTA update completely written! calling ota_update.complete() next.\n".as_bytes(),
    )?;
    ota_update.complete()?;
    warn!("OTA update completed, will reboot the chip in a second (in a spawned thread, to ensure http connection is completed)");
    thread::spawn(|| {
        delay::FreeRtos::delay_ms(1000);
        warn!("waited for a second, rebooting now!");
        unsafe { esp_restart() };
    });
    Ok(())
}
