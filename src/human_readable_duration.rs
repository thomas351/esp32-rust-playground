use core::fmt;
use std::time::Duration;

pub struct HumanReadableDuration {
    weeks: u32,
    days: u8,
    hours: u8,
    minutes: u8,
    seconds: u8,
    microseconds: u32,
}

impl fmt::Display for HumanReadableDuration {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match (self.weeks, self.days, self.hours, self.minutes) {
            (0, 0, 0, 0) => write!(f, "{}.{:06} seconds", self.seconds, self.microseconds),
            (0, 0, h, m) => write!(
                f,
                "{:02}:{:02}:{:02}.{:06}",
                h, m, self.seconds, self.microseconds
            ),
            (0, d, h, m) => write!(
                f,
                "{}d {:02}:{:02}:{:02}.{:06}",
                d, h, m, self.seconds, self.microseconds
            ),
            (w, d, h, m) => write!(
                f,
                "{}w {}d {:02}:{:02}:{:02}.{:06}",
                w, d, h, m, self.seconds, self.microseconds
            ),
        }
    }
}

pub fn format_duration(duration: Duration) -> HumanReadableDuration {
    let secs = duration.as_secs();
    let (mins, secs) = (secs / 60, secs % 60);
    let (hours, mins) = (mins / 60, mins % 60);
    let (days, hours) = (hours / 24, hours % 24);
    let (weeks, days) = (days / 7, days % 7);

    HumanReadableDuration {
        weeks: weeks as u32,
        days: days as u8,
        hours: hours as u8,
        minutes: mins as u8,
        seconds: secs as u8,
        microseconds: duration.subsec_micros(),
    }
}
