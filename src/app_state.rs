use crate::{bme280_handler::MeasurementVariable, Measurement};
use anyhow::anyhow;
use bme280_rs::Bme280;
use esp_idf_svc::{
    eventloop::{EspEventLoop, EspSystemEventLoop},
    hal::{
        delay,
        gpio::{Gpio19, Gpio21, Gpio22, Output, PinDriver},
        i2c::{I2cConfig, I2cDriver, I2C0},
        task::thread::ThreadSpawnConfiguration,
        units::MegaHertz,
    },
    sys::{esp_timer_get_time, va_list},
    timer::{EspTimer, EspTimerService, Task},
    wifi::EspWifi,
};
use log::error;
use once_cell::sync::{Lazy, OnceCell};
use parking_lot::{lock_api::RwLockWriteGuard, RawRwLock, RwLock};
use shared_bus::BusManager;
use std::{collections::HashMap, ffi::c_char, sync::Mutex, thread::JoinHandle, time::Duration};

cfg_if! {if #[cfg(feature = "touch")] {
    use crate::config::{CONFIG, DERIVED_CONFIG};
    use log::{debug, info, warn};
}}
#[cfg(all(feature = "display", feature = "touch"))]
use crate::display_handler::DISPLAY_HANDLER;

type Bme280Driver =
    Bme280<shared_bus::I2cProxy<'static, Mutex<I2cDriver<'static>>>, delay::FreeRtos>;

type NamedThreads = HashMap<&'static str, Option<JoinHandle<()>>>;

#[derive(Debug)]
pub struct Bme280RelayRule {
    pub variable: MeasurementVariable,
    pub value: f32,
    pub bigger: bool,
    pub relay_target: bool,
}
struct Relay {
    pin: Option<Gpio19>,
    driver: Option<PinDriver<'static, Gpio19, Output>>,
}

impl Relay {
    fn set(&mut self, active: bool) -> anyhow::Result<()> {
        let mut driver = match self.driver.take() {
            Some(driver) => driver,
            None => {
                let pin = self.pin.take().ok_or(anyhow!(
                    "Relay struct doesn't have driver nor pin, bad state!"
                ))?;
                PinDriver::output(pin)?
            }
        };
        match active {
            true => driver.set_high()?,
            false => driver.set_low()?,
        }
        self.driver = Some(driver);
        Ok(())
    }
}
// TODO: FIXME: replace Mutex by parking_lot's RwLock - where multiple parallel readers would be beneficial
pub struct AppState {
    pub timer_service: Lazy<EspTimerService<Task>>,
    pub timers: Mutex<Vec<EspTimer<'static>>>,
    pub threads: RwLock<Option<NamedThreads>>,
    pub sysloop: Lazy<EspEventLoop<esp_idf_svc::eventloop::System>>,
    pub wifi: Mutex<OnceCell<EspWifi<'static>>>,
    pub wifi_time: RwLock<Option<i64>>,
    pub bme280_driver: RwLock<Option<Bme280Driver>>,
    pub bme280_buffer: RwLock<([Option<Measurement>; 128], usize)>,
    pub bme280_time: RwLock<Option<i64>>,
    pub bme280_read_time: RwLock<Option<i64>>,
    bme280_failures: Mutex<usize>,
    pub bme280_relay_rules: RwLock<Vec<Bme280RelayRule>>,
    pub ota_time: RwLock<Option<i64>>,
    pub default_esp_logger: OnceCell<unsafe extern "C" fn(*const c_char, va_list) -> i32>,
    pub mac: OnceCell<heapless::String<12>>,
    i2c_bus: OnceCell<&'static BusManager<Mutex<I2cDriver<'static>>>>,
    #[cfg(feature = "touch")]
    buttons_active: RwLock<[bool; 10]>,
    #[cfg(all(feature = "display", feature = "touch"))]
    pub display_touches: RwLock<bool>,
    relay: Mutex<OnceCell<Relay>>,
    pub relay_active: RwLock<bool>,
    pub esp_log_vararg_parsing: RwLock<bool>,
}

impl AppState {
    const fn instance() -> Self {
        AppState {
            timer_service: Lazy::new(|| EspTimerService::new().unwrap()),
            timers: Mutex::new(Vec::new()),
            threads: RwLock::new(None),
            sysloop: Lazy::new(|| EspSystemEventLoop::take().unwrap()),
            wifi: Mutex::new(OnceCell::new()),
            wifi_time: RwLock::new(None),
            bme280_driver: RwLock::new(None),
            bme280_buffer: RwLock::new(([None; 128], 0)),
            bme280_time: RwLock::new(None),
            bme280_read_time: RwLock::new(None),
            bme280_failures: Mutex::new(0),
            bme280_relay_rules: RwLock::new(Vec::new()),
            ota_time: RwLock::new(None),
            default_esp_logger: OnceCell::new(),
            mac: OnceCell::new(),
            i2c_bus: OnceCell::new(),
            #[cfg(feature = "touch")]
            buttons_active: RwLock::new([false; 10]),
            #[cfg(all(feature = "display", feature = "touch"))]
            display_touches: RwLock::new(false),
            relay: Mutex::new(OnceCell::new()),
            relay_active: RwLock::new(false),
            esp_log_vararg_parsing: RwLock::new(false),
        }
    }

    pub fn bme280_last_block(&self) -> Option<Measurement> {
        let guard = self.bme280_buffer.read();
        guard.0[guard.1]
    }

    #[cfg(feature = "display")]
    pub fn bme280_last(&self) -> Option<Measurement> {
        self.bme280_buffer.try_read().and_then(|x| x.0[x.1])
    }

    pub fn bme280_failed(&self) {
        match self.bme280_failures.lock() {
            Err(e) => error!("app_state.bme280_failed Mutex poisoned! {e}"),
            Ok(mut guard) => {
                *guard += 1;
            }
        };
    }

    #[cfg(feature = "display")]
    pub fn bme280_history(
        &self,
    ) -> parking_lot::lock_api::RwLockReadGuard<'_, RawRwLock, ([Option<Measurement>; 128], usize)>
    {
        self.bme280_buffer.read()
    }

    pub fn bme280_push(&self, m: Measurement) {
        let mut guard = self.bme280_buffer.write();
        let mut guard_failures = self.bme280_failures.lock().unwrap();

        for rule in self.bme280_relay_rules.read().iter() {
            let value = m.get_variable(&rule.variable);
            if rule.bigger && value > rule.value || !rule.bigger && value <= rule.value {
                if let Err(e) = self.relay_toggle(Some(rule.relay_target)) {
                    error!("relay_toggle failed! {e}");
                }
                break;
            }
        }

        let mut i = (guard.1 + 1) % 128;
        if *guard_failures > 0 {
            guard.0[i..(i + *guard_failures)].fill(None);
            i = (i + *guard_failures) % 128;
            *guard_failures = 0;
        }
        guard.0[i] = Some(m);
        guard.1 = i;
        self.bme280_time
            .write()
            .replace(unsafe { esp_timer_get_time() });
    }

    pub fn mac(&self) -> &heapless::String<12> {
        self.mac
            .get_or_init(move || heapless::String::try_from("MAC_unknown").unwrap())
    }

    pub fn set_mac(&self, mac: heapless::String<12>) -> Result<(), heapless::String<12>> {
        self.mac.set(mac)
    }

    pub fn relay_init(&self, pin: Gpio19) -> anyhow::Result<()> {
        self.relay
            .try_lock()
            .map_err(|error| {
                anyhow!("couldn't acquire lock on app_state.relay mutex, init failed: {error}")
            })?
            .set(Relay {
                pin: Some(pin),
                driver: None,
            })
            .map_err(|_| anyhow!("couldn't store relay driver in relay onceCell! unbelievable!"))
    }

    pub fn relay_toggle(&self, target: Option<bool>) -> anyhow::Result<bool> {
        let mut active = self.relay_active.write();
        if let Some(specific) = target
            && specific == *active
        {
            debug!("not toggling relay, because it's current state is the same as the requested target");
            return Ok(*active);
        }

        let mut guard = self.relay.try_lock().map_err(|error| {
            anyhow!("couldn't acquire lock on app_state.relay mutex, init failed: {error}")
        })?;
        let relay = guard.get_mut().ok_or(anyhow!("relay cell was empty!"))?;

        relay.set(!*active)?;
        *active = !*active;
        Ok(*active)
    }

    #[cfg(feature = "touch")]
    pub fn touched(&self, touched: &[u16]) {
        let mut buttons_active = match self.buttons_active.try_write() {
            Some(g) => g,
            None => {
                warn!("couldn't acquire write lock on touch store, ignoring measurement.");
                return;
            }
        };

        for (pad, old) in buttons_active.iter_mut().enumerate() {
            let new = touched[pad];
            if new == 0 {
                debug!("touch pad, filtered[{pad}]=0 -> invalid value, won't consider it!");
                continue;
            }
            let limit_inactive = *DERIVED_CONFIG
                .button_threshold_inactive_custom
                .get(&pad)
                .unwrap_or(&CONFIG.button_threshold_inactive_default);
            let limit_active = *DERIVED_CONFIG
                .button_threshold_active_custom
                .get(&pad)
                .unwrap_or(&CONFIG.button_threshold_active_default);
            if *old && new > limit_inactive {
                info!("pad#{pad} was active, deactivated: {new}>{limit_inactive}");
                *old = false;
            }
            if !*old && new < limit_active {
                info!("pad#{pad} was inactive, activated: {new}<{limit_active}");
                *old = true;
                #[cfg(feature = "display")]
                if !*self.display_touches.read()
                    && let Some(function) = DERIVED_CONFIG.button_functions.get(&pad)
                    && let Some(selector) = DISPLAY_HANDLER.mode_selector.get()
                {
                    selector.try_send(*function).ok();
                }
            }
        }
        #[cfg(feature = "display")]
        if *self.display_touches.read() {
            DISPLAY_HANDLER.display_touch(touched, buttons_active.as_ref());
        }
    }

    pub fn schedule(
        &self,
        duration: Duration,
        regular: bool,
        callback: impl FnMut() + Send + 'static,
    ) {
        match self.timer_service.timer(callback) {
            Ok(timer) => {
                if regular && let Err(error) = timer.every(duration) {
                    error!("couldn't set timer's frequency: {:?}", error)
                } else if !regular && let Err(error) = timer.after(duration) {
                    error!("couldn't set timer's delay: {:?}", error)
                }
                match self.timers.try_lock() {
                    Ok(mut timers) => timers.push(timer),
                    Err(error) => {
                        error!("couldn't acquire APP_STATE.timers lock: {:?}", error)
                    }
                }
            }
            Err(error) => {
                error!("timer_service.timer creation failed: {:?}", error)
            }
        }
    }

    pub fn update_wifi_time(&self) {
        match self.wifi_time.try_write() {
            Some(mut guard) => {
                guard.replace(unsafe { esp_timer_get_time() });
            }
            None => error!("failed to store wifi setup timestamp, couldn't acquire write_lock!"),
        }
    }

    pub fn ota_started(&self) {
        self.ota_time
            .write()
            .replace(unsafe { esp_timer_get_time() });
    }

    pub fn ota_canceled(&self) {
        self.ota_time.write().take();
    }

    pub fn i2c(
        &self,
    ) -> Option<shared_bus::I2cProxy<'static, std::sync::Mutex<I2cDriver<'static>>>> {
        self.i2c_bus.get().map(|bus| bus.acquire_i2c())
    }

    pub fn init_i2c_bus(&self, i2c: I2C0, sda: Gpio21, scl: Gpio22) {
        let i2c_config = I2cConfig::new().baudrate(MegaHertz(1).into());
        let i2c_driver = match I2cDriver::new(i2c, sda, scl, &i2c_config) {
            Ok(i2c) => i2c,
            Err(error) => {
                error!("I2cDriver initialization failed: {error}");
                return;
            }
        };

        match shared_bus::new_std!(I2cDriver = i2c_driver) {
            Some(bus) => {
                if self.i2c_bus.set(bus).is_err() {
                    error!("couldn't store newly created i2c bus in OnceCell! did you try to init it multiple times?!");
                }
            }
            None => error!("Shared I2C Bus initialization failed!"),
        };
    }

    // pub fn setup_i2c(&self, i2c: I2C0, sda: Gpio21, scl: Gpio22) -> anyhow::Result<()> {
    //     trace!("defining i2c_config");
    //     let i2c_config = I2cConfig::new(); //.baudrate(KiloHertz(100).into());
    //     trace!("creating I2cDriver");
    //     self.i2c_driver.lock().unwrap().set(I2cDriver::new(i2c, sda, scl, &i2c_config)?).ok();
    //     Ok(())
    // }
}

pub fn threads() -> RwLockWriteGuard<'static, RawRwLock, Option<NamedThreads>> {
    let mut guard = APP_STATE.threads.write();
    if guard.is_none() {
        guard.replace(HashMap::new());
    }
    guard
}

pub fn start_thread<F: FnOnce() + Send + 'static>(
    name: &'static str,
    priority: u8,
    stack_size: usize,
    runner: F,
) {
    let thread_conf = ThreadSpawnConfiguration {
        name: Some(name.as_bytes()),
        priority,
        stack_size, // need to repeat in builder, see bug-report https://github.com/esp-rs/esp-idf-hal/issues/233
        inherit: false,
        ..Default::default()
    };
    if let Err(error) = thread_conf.set() {
        error!("AppState.start_thread: {name}: threadConf.set() failed for thread {error}");
    }
    match std::thread::Builder::new()
        .stack_size(stack_size)
        .spawn(runner)
    {
        Ok(handle) => {
            let mut lock = threads();
            lock.as_mut().unwrap().insert(name, Some(handle));
        }
        Err(error) => error!("AppState.start_thread: {name}: spawn() failed: {error}"),
    };
}

pub fn take_join_handle() -> (&'static str, Option<JoinHandle<()>>) {
    let mut guard = threads();
    for (name, handle) in guard.as_mut().unwrap().iter_mut() {
        if handle.is_some() {
            return (name, handle.take());
        }
    }
    ("None", None)
}

pub static APP_STATE: AppState = AppState::instance();
