cargo build --release --features display
cd target/xtensa-esp32-espidf/release/
espflash save-image --chip esp32 esp32-socket_rs extracted

echo "
To determine which ota partition is currently selected to boot,
look at the chips output on boot-up. line 22 of the green (info) boot-loader block will read:
I (619) boot: Loaded app from partition at offset 0x*00000
0x100000 = ota_0 & 0x200000 = ota_1
"
read -p "Press 0 to write to ota_0 (addr 0x100000) or 1 for ota_1 (addr 0x200000)
" choice
case "$choice" in 
  0 ) espflash write-bin 0x100000 extracted;;
  1 ) espflash write-bin 0x200000 extracted;;
  * ) echo "invalid choice, only 0 or 1.";;
esac
