sudo chmod 777 /dev/ttyUSB0

# on arch / manjaro we could simply use the distros tio package, on debian / ubuntu based distros it's to old and doesn't allow toggling serial lines.
# download and extract: https://github.com/tio/tio/releases
cd ~/Downloads/tio-2.5/build/src/
./tio /dev/ttyUSB0
# ctrl-t + g + 0 # serial line toggle: DTR to LOW = IO0 Pin, LOW = default boot, HIGH = flash mode
# ctrl-t + g + 1 # serial line toggle: RTS to LOW = Chip-Enabled Pin, strangly reversed meaning: LOW = enabled = pulled to VCC, HIGH = disabled = pulled to ground
