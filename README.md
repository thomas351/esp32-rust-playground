# Welcome

to my ESP32 BME280 Rust playground :)

## My story

I've previously programmed my ESP8266 & ESP32 boards using Arduino + PlatformIO, but since I'm a Rust fan and I suspect some memory leaks in my Arduino-style code (since my devices sometimes get slow / unresponsive over time) I'm trying to migrate to Rust.

## My hardware

The board I've prepared for learning to program future projects with Rust is this one:

``` log
$ cargo espflash board-info
Serial port: /dev/ttyUSB0
Connecting...

Chip type:         ESP32 (revision 1)
Crystal frequency: 40MHz
Flash size:        4MB
Features:          WiFi, BT, Dual Core, 240MHz, VRef calibration in efuse, Coding Scheme None
```

I've wired it to an USB-to-serial adapter that has a 3.3V mode I'm using to power my ESP32 board. Besides power those pins are connected:

* DTR <-> IO0
* RX <-> TX0
* TX <-> RX0
* R/C=RTS <-> EN

Those connections enable tools like the `espflash` above to reliably read / write / reset / .. the board as needed.

Reading <https://esp-rs.github.io/book/tooling/debugging/vscode-debugging.html> tells me my board does not have an included JTAG interface, so no debugging without additional hardware. I believe logging to serial connection will suffice for my needs for now though.

## Dev setup

``` bash
cargo install espup
espup install
cargo install ldproxy # required for build to work
sudo apt install pkg-config libudev-dev # required for espflash build
sudo apt install python3-venv #required for esp-idf build
cargo install espflash # required for extracting raw image from build, for ota_update
source ~/export-esp.sh
echo "source ~/export-esp.sh" >> ~/.profile # make it permanent, so in the future all you need to do is:
cargo run
```

## Observe currently installed image

<https://github.com/esp-rs/espmonitor>

``` bash
cargo espmonitor
```

This stops working for me when using the OTA partition layout, so instead I'm now using `tio /dev/ttyUSB0` and toggle DTR & RTS to low using `ctrl-t g 0` & `ctrl-t g 1` to make the chip boot. Sadly this doesn't work with the `tio` package that's in Debian based distributions (v1.32), so for that we need to manually download & build v2.5, which luckily is quite easy. See <https://github.com/tio/tio/releases/tag/v2.5>

## Features implemented so far

* We're connecting to the AccessPoint configured in `cfg.toml` (copy and adapt `cfg.toml.example`)
* While using `Mixed` mode to also function as AccessPoint at the same time with BSSID = ESP32_[mac-addr]. A password for that AP can also be configured in `cfg.toml`. This is to allow OTA updates even if the network we're trying to join as client station is unavailable, or we made an error in our wifi config.
* HTTP Server with a few get-handlers to retrieve information (uptime, heap, log, wifi_scan, bme280) and a put-handler to receive OTA-Updates (see ota_update.sh for the other side of it, and ota_setup.sh + partitions.csv for setup). Lastly there's also a get-handler for the path "reboot" which causes a system reboot.
* Logging to syslog server, see src/log_buffer.rs
* WiFi networks are no longer scanned (and printed) on startup because since an update to the esp-idf-* crates scanning before configuring wifi doesn't work anymore. We replaced that feature with <http://esp32/wifi_scan> http handler. Results are shown in http response and in the serial + syslog output.
* We're connecting to a BME280 sensor via I2C, making regular measurements with configurable frequency via `cfg.toml`, and sending the values to a VictoriaMetrics server (faster Timeseries Database than InfluxDB, with mostly the same features and APIs).
  * The bme280 library has been replaced for one compatible with [shared_bus](https://crates.io/crates/shared-bus)
  * Learned an amazing trick to find [projects that use a specific combination of dependencies, like in this case esp-idf-hal (=std esp projects) & shared_bus & bme280](https://github.com/search?q=path%3ACargo.toml+content%3Aesp-idf-hal+content%3Ashared-bus+content%3Abme280&type=code)
* An ssd1306 display has been connected to that shared i2c bus and we're displaying a temperature humidity and dew-point as a scrolling text on it. See also [this github issue](https://github.com/embedded-graphics/embedded-graphics/issues/646) for thoughts on the implementation detail and maybe possible improvements to it.
  * http methods to stop the scrolling which makes the display driver available for http methods and others to fill it with test patterns or clearing = turning it off have been added.
* We're also regularly checking and reporting our wifi signal strength and uptime in microseconds to VictoriaMetrics.
* Our initially used busy loop has been replaced by threads & timers:
  * Thread1: LogBuffer.receiver_loop: waits for crossbeam_channel::bounded to send log messages to syslog server and store them in buffers (log + problems if >=warning)
  * Thread2: main.data_sender for VictoriaMetrics receiver
  * Thread3: i2c handler: bme280 sensor reading + display scroll. bme280 was a timer initially, but since the I2C interface sometimes blocks the calling thread we moved it into it's own thread, so that not all timers which share a single thread get blocked from ever running again till reboot.
  * Timer1: Uptime sender / logger (To VictoriaMetrics and dependent on config maybe also to serial+syslogger)
  * Timer2: check_wifi: restarts & reconnects wifi if disconnected. Also sends RSSI to VictoriaMetrics & logs wifi stats as configured
  * Timer3: bme280_freeze_detector: reboots the chip if bme280 hasn't been read for more than twice the configured delay
  * Sysloop.subscriber on WifiEvents: logs events received and calls check_wifi on every WifiEvent.

## OTA references

* <https://crates.io/crates/esp-ota/0.1.0>
* <https://github.com/faern/esp-ota>
* <https://github.com/esp-rs/esp-idf-svc/blob/v0.44.0/src/ota.rs>
* <https://github.com/taunusflieger/anemometer/issues/12>

## partitioning

<https://docs.espressif.com/projects/esp-idf/en/latest/esp32/api-guides/partition-tables.html>

* 4mb = 4\*1024\*1024 = 4194304 bytes should be available
* currently our 2 ota_* partitions are 0x1F0000 = 2031616 bytes big each.
* the offset of the first app image partition is forced to be 0x10000 = 65536 bytes.
* 4194304-65536 = 4128768 bytes left for app partitions.
* theoretical maximum size = 4128768/2 = 2064384 bytes = 0x1F8000. But:
  >Partitions of type app have to be placed at offsets aligned to 0x10000 (64 K)
* so 0x1F0000 is actually the biggest possible ota_* partition size that fits.
* I thought it should be possible to fit a data partition at the end and tried:
  >conf,    data, nvs,     0x3F0000, 0x3000,

  but with that, ota_setup.sh fails to flash:
  >Error: espflash::partition_table::invalid_partition_table

## Todo-list / open issues

* Implement touch sensors:
  * <https://docs.espressif.com/projects/esp-idf/en/latest/esp32/api-reference/peripherals/touch_pad.html#touch-state-measurements>
  * <https://github.com/ESP32DE/esp-iot-solution-1/blob/master/documents/touch_pad_solution/touch_sensor_design_en.md>
  * <https://randomnerdtutorials.com/esp32-touch-pins-arduino-ide/>
  * <https://esp32.com/viewtopic.php?t=5055>
  * <https://www.reddit.com/r/esp32/comments/140ggv1/is_there_an_esprs_equivalent_of_the_arduino/>
* esp_log_processor for now only logs the pattern without it's arguments applied. See also:
  * <https://github.com/esp-rs/rust/issues/177>
  * <https://github.com/esp-rs/esp-wifi/issues/16>
  * <https://github.com/esp-rs/esp-idf-sys/issues/212>
  * <https://users.rust-lang.org/t/esp-idf-logger-redirection-vprintf-variadic-function/95568>
  * <https://docs.rs/printf-compat/latest/printf_compat>
* Test wether the AP mode still works when the configured client mode doesn't (maybe some of those unwrap() calls make it crash in that case)
* Wifi FTM Session could give more detailed wifi info, but it seems it's not supported @ ESP32, only later chips. See also:
  * <https://docs.espressif.com/projects/esp-idf/en/v4.4/esp32/api-reference/network/esp_wifi.html>
  * <https://www.esp32.com/viewtopic.php?t=24226>
  * <https://github.com/espressif/arduino-esp32/blob/master/libraries/WiFi/examples/FTM/FTM_Initiator/FTM_Initiator.ino>
    > only supported by ESP32-S2 and ESP32-C3
