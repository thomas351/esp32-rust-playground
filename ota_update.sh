cargo build --release --features display
ota_target=$(grep -v "^#" cfg.toml | grep -oP 'ota_target = "\K.*"' | tr -d '"')
cd target/xtensa-esp32-espidf/release
espflash save-image --chip esp32 esp32-socket_rs extracted
echo "build done, sending to $ota_target"
curl -X PUT -T extracted $ota_target/ota