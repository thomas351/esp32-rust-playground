echo "
################################################################################
WARNING: setup will only write application to ota_0 partition, but leave otadata config partition unchanged.

If you're using this script to overwrite a previous version that's broken beyond ota_update capabilities and you're unlucky (50/50 chance),
otadata config partition might point the boot-loader to boot ota_1 which will still contain that old broken version!

to determine which ota partition will boot, look at the chips output when it boots, around line 34 will read either:
I (619) boot: Loaded app from partition at offset 0x100000   --> ota_0, or:
I (619) boot: Loaded app from partition at offset 0x200000   --> ota_1

to write to one specific ota partition directly and skip rewriting the system parts @ 0x1000 & 0x8000
use ota_repair.sh instead
################################################################################

"
cargo build --release --features display
espflash flash --monitor --partition-table partitions.csv target/xtensa-esp32-espidf/release/esp32-socket_rs
