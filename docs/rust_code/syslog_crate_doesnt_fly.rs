
// syslog = "6.0.1"
use syslog::{BasicLogger, Facility, Formatter3164};

fn main() {
    let syslog_formatter = Formatter3164 {
        facility: Facility::LOG_USER,
        hostname: Some(format!("ESP32_{mac}")),
        process: "main".into(),
        pid: 0,
    };
    match syslog::tcp(syslog_formatter, ("192.168.16.4", "514").to_socket_addrs()) {
        Err(e) => println!("could not connect to syslog: {:?}", e),
        Ok(logger) => log::set_boxed_logger(Box::new(BasicLogger::new(logger))),
    };
}

// https://github.com/Geal/rust-syslog/issues/72

/*

with version >=6.0.0 compilation fails like that:

error[E0425]: cannot find value `_SC_HOST_NAME_MAX` in crate `libc`
  --> /home/thomas/.cargo/registry/src/github.com-1ecc6299db9ec823/hostname-0.3.1/src/nix.rs:17:38
   |
17 |         unsafe { libc::sysconf(libc::_SC_HOST_NAME_MAX) as libc::size_t };
   |                                      ^^^^^^^^^^^^^^^^^ not found in `libc`

and with version <= 5.0.0 like that:

error[E0609]: no field `tm_gmtoff` on type `tm`
   --> /home/thomas/.cargo/registry/src/github.com-1ecc6299db9ec823/time-0.1.45/src/sys.rs:392:30
    |
392 |             let gmtoff = out.tm_gmtoff;
    |                              ^^^^^^^^^ unknown field

 */