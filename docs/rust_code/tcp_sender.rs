use crossbeam_channel::{bounded, Receiver, Sender};
use log::{Level, Metadata, Record};
use std::{
    io::{self, Write},
    net::{SocketAddr, TcpStream, ToSocketAddrs},
    thread::JoinHandle,
    time::Duration,
};

use crate::app_state::APP_STATE;

pub struct TcpSender {
    tx: Sender<String>,
    _sender: JoinHandle<()>,
    buffer_filter: Level,
}

impl TcpSender {
    pub fn new(host: &str, port: u16, buffer_filter: Level) -> io::Result<TcpSender> {
        println!("TcpSender: creating instance!");
        match (host, port).to_socket_addrs()?.next() {
            Some(socket_addr) => {
                println!("TcpSender: socket definition resolved, defining queue!");
                let (tx, rx) = bounded(64);
                Ok(TcpSender {
                    tx,
                    _sender: spawn_tcp_thread(socket_addr, rx)?,
                    buffer_filter,
                })
            }
            None => Err(io::Error::new(
                io::ErrorKind::NotFound,
                "TcpSender: couldn't resolve socket specification",
            )),
        }
    }

    pub fn send(&self, message: String) {
        if let Err(error) = self.tx.try_send(message) {
            eprintln!("TcpSender, message dropped: {error}");
        };
    }
}

fn try_send(stream: &mut TcpStream, message: &str) -> Result<(), io::Error> {
    stream.write_all(message.as_bytes())?;
    stream.write_all(&[b'\n'])?;
    // writeln!(stream, "ESP32 {} {message}", APP_STATE.mac())?;
    stream.flush()?;
    Ok(())
}

// preserve unused function and prevent the compiler from warning that it's unused. we might want to switch back to it
#[allow(dead_code)]
fn send_with1reconnect(
    stream: &mut Option<TcpStream>,
    addr: &SocketAddr,
    message: &str,
) -> Result<(), io::Error> {
    let new_connection = stream.is_none();
    if new_connection {
        stream.replace(TcpStream::connect_timeout(addr, Duration::from_secs(5))?);
    };
    if let Err(error) = try_send(stream.as_mut().unwrap(), message) {
        if new_connection {
            return Err(error);
        } else {
            stream.replace(TcpStream::connect_timeout(addr, Duration::from_secs(5))?);
            try_send(stream.as_mut().unwrap(), message)?;
        }
    }
    Ok(())
}

fn send(store: &mut Option<TcpStream>, addr: &SocketAddr, message: &str) {
    loop {
        if store.is_none() {
            match TcpStream::connect_timeout(addr, Duration::from_secs(5)) {
                Ok(stream) => store.replace(stream),
                Err(error) => {
                    eprintln!("TcpSender.send() failed to connect to {addr}: {error}");
                    continue;
                }
            };
        }
        if let Err(error) = try_send(store.as_mut().unwrap(), message) {
            eprintln!("TcpSender.send() failed: {error}");
            store.take();
        } else {
            return;
        }
    }
}

// SyslogSender needs to be a thread and can't be coded as timer, because it waits for an interrupt and doesn't run in a regular interval.
fn spawn_tcp_thread(
    addr: SocketAddr,
    rx: Receiver<String>,
) -> Result<std::thread::JoinHandle<()>, io::Error> {
    println!("TcpSender: spawning sender thread");
    let threadConf = ThreadSpawnConfiguration {
        name: Some("tcp_sender\0".as_bytes()),
        priority: 2,
        stack_size: CONFIG.stack_size_tcp_sender, // need to repeat in builder, see bug-report https://github.com/esp-rs/esp-idf-hal/issues/233
        ..Default::default()
    };
    if let Err(error) = threadConf.set() {
        eprintln!("tcp_sender.spawn_tcp_thread threadConf.set() failed: {error}");
    }
    std::thread::Builder::new()
        .name("tcp_sender".into())
        .stack_size(CONFIG.stack_size_tcp_sender)
        .spawn(move || {
            println!("TcpSender: sender thread start");
            let mut stream_store: Option<TcpStream> = None;
            loop {
                match rx.recv() {
                    Ok(message) => {
                        send(&mut stream_store, &addr, &message);
                        // if let Err(error) = try_send_with1reconnect(&mut stream_store, &addr, &message) {
                        //     eprintln!("TcpSender.try_send_with_reconnect failed: {error} ; skipping message: {message}");
                        //     stream_store = None;
                        // };
                    }
                    Err(error) => eprintln!("couldn't receive message from queue! {error}"),
                };
            }
        })
}

fn get_marker(level: Level) -> &'static str {
    match level {
        Level::Error => "E",
        Level::Warn => "W",
        Level::Info => "I",
        Level::Debug => "D",
        Level::Trace => "V",
    }
}

fn get_color(level: Level) -> Option<u8> {
    match level {
        Level::Error => Some(31), // LOG_COLOR_RED
        Level::Warn => Some(33),  // LOG_COLOR_BROWN
        Level::Info => Some(32),  // LOG_COLOR_GREEN,
        _ => None,
    }
}

impl log::Log for TcpSender {
    fn enabled(&self, _metadata: &Metadata) -> bool {
        true
    }

    fn log(&self, record: &Record) {
        if self.enabled(record.metadata()) {
            let marker = get_marker(record.level());
            let module = record.module_path().unwrap_or("unknown");
            let file = record.file().unwrap_or("");
            let line = record.line().unwrap_or(0);
            let raw_msg = record.args();
            let local_msg = format!("{marker} [{module}] ({file}:{line}) {raw_msg}");
            let remote_msg = format!("ESP32 {} {local_msg}", APP_STATE.mac());
            match get_color(record.level()) {
                Some(color) => {
                    println!("\x1b[0;{color}m{local_msg}\x1b[0m");
                    // self.send(format!("\x1b[0;{color}m{remote_msg}\x1b[0m")).ok();
                }
                None => {
                    println!("{local_msg}");
                }
            };
            if record.level() <= self.buffer_filter {
                match APP_STATE.log_buffer.lock() {
                    Ok(mut buffer) => buffer.push_line(&local_msg),
                    Err(error) => eprintln!("log buffer poisoned! {error}"),
                }
            }
            self.send(remote_msg);
        }
    }

    fn flush(&self) {}
}
