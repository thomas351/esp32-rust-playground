fn validate_bme280_diff(
    old: &Measurements<I2cError>,
    new: Measurements<I2cError>,
    expected_temperature_step: f32,
    expected_humidity_step: f32,
    expected_pressure_step: f32,
) -> Result<Measurements<I2cError>, Option<(f32, f32, f32)>> {
    let diff_temperature = (old.temperature - new.temperature).abs();
    let diff_humidity = (old.humidity - new.humidity).abs();
    let diff_pressure = (old.pressure - new.pressure).abs();

    let suspect_temperature = diff_temperature > expected_temperature_step;
    let suspect_humidity = diff_humidity > expected_humidity_step;
    let suspect_pressure = diff_pressure > expected_pressure_step;

    if suspect_temperature || suspect_humidity || suspect_pressure {
        warn!("bme280 invalid, diff too big: temperature={suspect_temperature}, humidity={suspect_humidity}, pressure={suspect_pressure}");

        let next_temperature_step = expected_temperature_step
            + (expected_temperature_step * 0.1 * suspect_temperature as u8 as f32);
        let next_humidity_step =
            expected_humidity_step + (expected_humidity_step * 0.1 * suspect_humidity as u8 as f32);
        let next_pressure_step =
            expected_pressure_step + (expected_pressure_step * 0.1 * suspect_pressure as u8 as f32);

        info!(
            "temperature step: expected={expected_temperature_step}, observed={diff_temperature}, next={next_temperature_step}"
        );
        info!(
            "humidity step expected={expected_humidity_step}, observed={diff_humidity}, next={next_humidity_step}"
        );
        info!(
            "pressure step expected={expected_pressure_step}, observed={diff_pressure}, next={next_pressure_step}"
        );
        Err(Some((
            next_temperature_step,
            next_humidity_step,
            next_pressure_step,
        )))
    } else {
        Ok(new)
    }
}

fn read_and_validate_bme280(
    expected_temperature_step: f32,
    expected_humidity_step: f32,
    expected_pressure_step: f32,
) -> Result<Measurements<I2cError>, Option<(f32, f32, f32)>> {
    trace!("read_and_validate_bme280 start");

    match read_bme280_try() {
        Ok(measurements) => {
            info!("{:?}", measurements);
            match APP_STATE.bme280_last.try_lock() {
                Ok(last_lock) => match last_lock.as_ref() {
                    Some((last, _time)) => {
                        info!("comparing to last measurements: {:?}", last);
                        validate_bme280_diff(
                            last,
                            measurements,
                            expected_temperature_step,
                            expected_humidity_step,
                            expected_pressure_step,
                        )
                    }
                    None => {
                        warn!("can't validate measurements, no previous measurements available for comparison!");
                        Ok(measurements)
                    }
                },
                Err(error) => {
                    error!("APP_STATE.bme280_last.try_lock() failed: {error}");
                    Err(None)
                }
            }
        }
        Err(error) => {
            error!("{error}");
            Err(None)
        }
    }
}

// read_bme280_until_valid(&tx_clone);
fn read_bme280_until_valid(tx: &Sender<String>) {
    trace!("read_bme280_until_valid: started");

    let mut tried = 0;
    let mut expected_temperature_step = CONFIG.expected_temperature_step;
    let mut expected_humidity_step = CONFIG.expected_humidity_step;
    let mut expected_pressure_step = CONFIG.expected_pressure_step;
    trace!("bme280 timer: variables defined");
    while tried <= ((CONFIG.bme280_delay_ms / CONFIG.bme280_retry_ms) - 2) {
        trace!("bme280 timer: retry-loop start");
        match read_and_validate_bme280(
            expected_temperature_step,
            expected_humidity_step,
            expected_pressure_step,
        ) {
            Ok(measurements) => {
                trace!("bme280 timer: got valid measurements");
                send_measurements(&measurements, tx);
                match APP_STATE.bme280_last.try_lock() {
                    Ok(mut bme280_last) => {
                        bme280_last.replace((measurements, unsafe { esp_timer_get_time() }));
                    }
                    Err(error) => error!("APP_STATE.bme280_last.try_lock failed: {error}"),
                }
                return;
            }
            Err(error) => {
                if let Some(result) = error {
                    expected_temperature_step = result.0;
                    expected_humidity_step = result.1;
                    expected_pressure_step = result.2;
                }
            }
        }
        tried += 1;
        delay::FreeRtos::delay_ms(CONFIG.bme280_retry_ms);
        info!("bme280 timer: retry-loop end reached, tried={tried}");
    }
    warn!("bme280 timer: reached maximum retries, hope we'll have better luck next time? verification algorithm needs improvement!");
}

// fn read_and_send_bme280(tx: &Sender<String>) {
//     match read_bme280_validated() {
//         Ok(measurements) => {}
//         Err(error) => warn!("failed to read bme280: {error}"),
//     }
// }

// let (first, tried) = read_bme280_retry(tries_left)?;
// tries_left -= tried;

// if let Some(last_accepted) = last_accepted_guard.as_ref() {
//     // if bme280_plausible_diff(&last_accepted.0, &first) {
//     //     return Ok(first);
//     // }
//     // measurements.push(first);
//     return read_bme280_validated_followed(measurements, tries_left, &last_accepted.0);
// }
// // measurements.push(first);
// return read_bme280_validated_first(measurements, tries_left);

// -> Measurements<I2cError> {

// ideas for a new faster and more robust validation algorithm:
// for our first measurement we should take multiple measurements:
//  * at least 2 initially and see if they are within expected ranges of each other. If so use their average.
//  * if not, take a 3rd and see if two of those 3 are within expected ranges of each other. If so use their average.
// for all other measurements, just take one initially and same as before accept it if it's within range of the last accepted one.
// if not repeat same procedure with a second measurement.
// if that doesn't fit either, take a 3rd and if this one also isn't within range of the last accepted one:
//  * see if the 3 new ones are within range of each other. if so, take their average
//  * else see if 2 of those 3 are within range of each other. if so, take their average

// let mut measurements: Vec<Measurements<I2cError>> = vec![];

// let mut tries_left = (CONFIG.bme280_delay_ms / CONFIG.bme280_retry_ms) - 2;

// // we will need one measurement in any case, let's get that one first.
// let (first, tried) = read_bme280_retry(tries_left)?;
// tries_left -= tried;

// match last_accepted_guard.as_ref() {
//     Some(last_accepted) => {
//         if bme280_plausible_diff(&last_accepted.0, &first) {
//             // let first_ref = &first;
//             // last_accepted_guard.replace((first, unsafe { esp_timer_get_time() }));
//             // Ok(first_ref)
//             Ok(first)
//         } else {
//             delay::FreeRtos::delay_ms(CONFIG.bme280_retry_ms);
//             let (mut second, tried) = read_bme280_retry(tries_left)?;
//             tries_left -= tried;
//             if bme280_plausible_diff(&last_accepted.0, &second) {
//                 Ok(second)
//             } else if bme280_plausible_diff(&first, &second) {
//                 second.temperature = (first.temperature + second.temperature) / 2.0;
//                 second.pressure = (first.pressure + second.pressure) / 2.0;
//                 second.humidity = (first.humidity + second.humidity) / 2.0;
//                 Ok(second)
//             } else {
//                 Err(
//                     "first & second measurements too far off from last_accepted and from each other. todo, implement"
//                         .into(),
//                 )
//             }
//         }
//         // let mut expected_humidity_step = CONFIG.expected_humidity_step;
//         // let mut expected_pressure_step = CONFIG.expected_pressure_step;
//         // measurements.append(last_accepted),
//     }
//     None => read_bme280_validated_first(),
// }

//  loop {
//     match read_bme280_try() {
//         Ok(measurement) => measurement,
//         Err(error) => error!("failed to read bme280 sensor! {error}"),
//     };
//     tried += 1;
//     delay::FreeRtos::delay_ms(CONFIG.bme280_retry_ms);
// };
// let first = measurements.get(0).unwrap();
