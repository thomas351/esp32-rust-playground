// flexi_logger = "0.25.1"

use flexi_logger::{writers::LogWriter, DeferredNow};
use std::{
    io::{Error, ErrorKind, Write},
    net::{SocketAddr, TcpStream},
    sync::Mutex, time::Duration,
};
 
pub struct TcpLogWriter {
    socket_addr: SocketAddr,
    stream: Mutex<Option<TcpStream>>,
}

impl TcpLogWriter {
    pub fn new(socket_addr: SocketAddr) -> TcpLogWriter {
        TcpLogWriter {
            socket_addr,
            stream: Mutex::new(None),
        }
    }

    fn connect(&self) -> Result<(), Error> {
        println!("TcpLogWriter - connecting to: {:?}", self.socket_addr);
        let stream = TcpStream::connect_timeout(&self.socket_addr, Duration::from_millis(500))?;
        // println!("TcpLogWriter - stream connected: {:?}", stream);
        stream.set_nodelay(true)?;
        println!("TcpLogWriter - set_nodelay done: {:?}", stream);
        self.stream.lock().unwrap().replace(stream);
        Ok(())
    }

    fn write_to_stream(&self, message: &str) -> Result<(), Error> {
        let mut stream = self.stream.lock().unwrap();
        if stream.is_none() {
            self.connect()?;
        }
        if let Some(ref mut stream) = *stream {
            println!("TcpLogWriter - write_to_stream: {:?}", stream);
            stream.write_all(message.as_bytes())?;
            println!("TcpLogWriter - written_to_stream, now flushing: {:?}", stream);
            stream.flush()?;
            println!("TcpLogWriter - stream flushed: {:?}", stream);
            return Ok(());
        }
        Err(Error::new(ErrorKind::NotConnected, "couldnt write to socket"))
    }
}

impl LogWriter for TcpLogWriter {
    fn write(&self, now: &mut DeferredNow, record: &flexi_logger::Record) -> std::io::Result<()> {
        let message = format!(
            "{} [{}] {}: {}\n",
            now.now().format("%Y-%m-%d %H:%M:%S%.6f"),
            record.level(),
            record.module_path().unwrap_or("<unnamed>"),
            record.args()
        );
        print!("{message}");
        self.write_to_stream(&message)?;
        Ok(())
    }

    fn flush(&self) -> std::io::Result<()> {
        Ok(())
    }
}

fn usage() {
    let mut logger_handle = None;
    if let Ok(mut syslog_target) = (CONFIG.syslog_host, CONFIG.syslog_port).to_socket_addrs() {
        if let Some(syslog_target) = syslog_target.next() {
            let writer = TcpLogWriter::new(syslog_target);
            if let Ok(logger) = flexi_logger::Logger::try_with_str("debug") {
                // let _l2 =logger.log_to_writer(Box::new(writer));
                if let Ok(handle) = logger.log_to_writer(Box::new(writer)).start() {
                    logger_handle.replace(handle);
                }
            }
        }
    }
}