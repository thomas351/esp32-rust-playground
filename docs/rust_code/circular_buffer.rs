pub struct CircularBuffer<T> {
    buffer: Vec<T>,
    read_index: usize,
    write_index: usize,
}

impl<T: Clone> CircularBuffer<T> {
    pub fn new(size: usize, default: T) -> CircularBuffer<T> {
        CircularBuffer {
            buffer: vec![default; size],
            read_index: 0,
            write_index: 0,
        }
    }

    pub fn push(&mut self, item: T) {
        self.buffer[self.write_index] = item;
        self.write_index = (self.write_index + 1) % self.buffer.len();
        if self.write_index == self.read_index {
            self.read_index = (self.read_index + 1) % self.buffer.len();
        }
    }

    pub fn push_all(&mut self, items: Vec<T>) {
        for item in items {
            self.push(item);
        }
    }

    // pub fn push_all_array(&mut self, items: &[T]) {
    //     for item in items {
    //         self.push(item.clone());
    //     }
    // }

    pub fn read(&self) -> Vec<T> {
        let mut result = Vec::new();
        let mut index = self.read_index;
        while index != self.write_index {
            result.push(self.buffer[index].clone());
            index = (index + 1) % self.buffer.len();
        }
        result
    }
}
