fn test() {
    if let Ok(guard) = APP_STATE.default_esp_logger.lock() && let Some(default_esp_logger) = guard.as_ref() {
        unsafe {default_esp_logger(str, args);}
    }

    {
        let va_list1: VaListImpl<'_> = VaListImpl {
            stk: &mut (args[0] as i32),
            reg: &mut (args[1] as i32),
            ndx: args[2] as i32,
            _marker: std::marker::PhantomData,
        };
        let x = VaList::from_slice(&args);
    }

    let pattern = CStr::from_ptr(str);

    // let arg0_cstr = CStr::from_ptr(args[0] as *mut c_char);
    // let arg1_cstr = CStr::from_ptr(args[1] as *mut c_char);
    // let args_hex: String = args.iter().map(|&x| format!("{:x}", x)).collect();
    // let raw_ptr0 = (args[0] as *mut c_char);
    // let format_str = format_CStr.to_str().unwrap();
    // format!(format_str, args);
    APP_STATE.buffered_logger.store_and_send(
        format!("esp-idf: args={:?}, format={:?}", args_hex, format_cstr),
        match args[2] {
            16 => Level::Info,
            12 => Level::Debug,
            _ => Level::Warn,
        },
    );
}
