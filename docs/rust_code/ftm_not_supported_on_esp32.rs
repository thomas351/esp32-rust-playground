// https://www.esp32.com/viewtopic.php?t=19642
// > FTM is only supported on the ESP32-S2 and ESP32-C3. The ESP32 does not support FTM.
fn ftm_init(wifi: &mut MutexGuard<EspWifi>) {
    if let Ok(wifi::Configuration::Mixed(client, _)) = wifi.driver().get_configuration() {
        let mut cfg = wifi_ftm_initiator_cfg_t {
            resp_mac: wifi.ap_netif().get_mac().unwrap(),
            channel: client.channel.unwrap_or(0),
            frm_count: 0,
            burst_period: 0,
        };
        let result = unsafe{esp_wifi_ftm_initiate_session(&mut cfg)};
        println!("ftm session initialized, result={result}");
    }
}
// https://esp-rs.github.io/esp-idf-sys/esp_idf_sys/fn.esp_wifi_ftm_initiate_session.html
// ftm_init(&mut wifi);
