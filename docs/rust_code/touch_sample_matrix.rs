/* request:
Hey everyone!

I'm trying to add touch button functionality to my esp32 rust playground, and as far as I understood the relevant esp-idf documentation I found it should be enough to initialize it by calling `touch_pad_init()` & `touch_pad_filter_start(20u32)` and then afterwards read the current state of all touch buttons by calling `touch_pad_read_raw_data` & `touch_pad_read_filtered` - but no matter what I do, those always write 0 into the `&mut value: u16` pointer I'm passing to them.

I believe that no matter my current hardware design, I should at least get some random value between 500 and 3000 from those functions when leaving all the settings for measurement and interval cylces by their default, but 0 seems like I misunderstood the touch API or there's some bug / failure to enable something for esp-idf compilation - like do I need to add some options to `sdkconfig.defaults` or something?

Previously I've used ArduinoIDE to code that touch functionality and it was much easier there... Has anybody here tried / used the touch button functionality and can maybe give me some links / snippets / hints / tell me about your experience?

Thank you!*/

pub type TouchPadData = heapless::LinearMap<u8, u16, { TOUCHPADS.len() }>;

const TOUCHPADS: [u8; 4] = [2, 3, 4, 5];

/// how often the touch data is updated
///
/// **Caution: Don't reduce under 10ms, or it'll break**
const TOUCH_PAD_UPDATE_FREQ: Duration = Duration::from_millis(10);

static TOUCH_INITIALIZED: AtomicBool = AtomicBool::new(false);
static TOUCH_THRESHOLD: u16 = 100;

/// how much percent a new value influences the current one (for filtering)
static FILTER_PERCENT: u32 = 40;

/// Init Touch with a handler `FnMut(TouchNum, TouchButtonEvent)`
pub fn init_touch() -> anyhow::Result<Arc<Mutex<TouchPadData>>> {
    // if touch is already initialized
    if TOUCH_INITIALIZED.swap(true, Ordering::Acquire) {
        panic!("Touch already initialized!");
    }

    // touch pad init code
    let ret = unsafe { sys::touch_pad_init() };
    EspError::convert(ret)?;
    for touchpad in TOUCHPADS {
        let ret = unsafe { sys::touch_pad_config(touchpad.into(), TOUCH_THRESHOLD) };
        EspError::convert(ret)?;
    }

    // pre-fill map
    let map = TouchPadData::from_iter(TOUCHPADS.map(|p| (p, 0u16)));

    let touch_data = Arc::new(Mutex::new(map));

    {
        // thread move scope
        let touch_data = touch_data.clone();
        thread::spawn(move || loop {
            {
                // scope for map locking
                let mut map = touch_data.lock().expect("Couldn't unlock TouchData Mutex!");
                for touchpad in TOUCHPADS {
                    // read touch data
                    let mut val = 0u16;
                    let ret = unsafe { sys::touch_pad_read(touchpad.into(), &mut val as _) };
                    if ret != 0 {
                        println!(
                            "WARN: Touch Read for Pad {} returned non-zero {}! (touch val: {})",
                            touchpad,
                            EspError::convert(ret).unwrap_err(),
                            val
                        )
                    }

                    let previous_val = map.get(&touchpad).unwrap_or(&0u16);
                    let filtered = (
                        // (old * (100 - percent) + new * percent) / 100
                        ((*previous_val as u32) * (100 - FILTER_PERCENT)
                            + (val as u32) * FILTER_PERCENT)
                            / 100
                    ) as u16;
                    // write into data

                    map.insert(touchpad, filtered)
                        .expect("BUG: Touch Map Capacity is too small!");
                }
                // drop(map);  // alterative to scope
            } // map lock is dropped here

            thread::sleep(TOUCH_PAD_UPDATE_FREQ);
        })
    };

    Ok(touch_data)
}
