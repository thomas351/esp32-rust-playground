#![feature(maybe_uninit_uninit_array)]

use std::{
    cmp::Ordering::*,
    io::{self, Write},
    net::{SocketAddr, TcpStream, ToSocketAddrs},
    slice,
    sync::{LockResult, Mutex, MutexGuard},
    time::Duration,
};

use log::{debug, error, info, warn, Level};
use std::mem::MaybeUninit;

use crate::APP_STATE;

pub struct LogBuffer<const SIZE: usize> {
    buffer: [MaybeUninit<u8>; SIZE],
    last_sent: usize,
    write_position: usize,
    overflow: bool,
    filled: bool,
    // line: bool,
    syslog_target: Option<SocketAddr>,
    syslog_stream: Option<TcpStream>,
}

fn syslog_prefix(stream: &mut TcpStream) -> Result<(), io::Error> {
    stream.write_all(b"ESP32 ")?;
    stream.write_all(APP_STATE.mac().as_bytes())?;
    stream.write_all(b" ")
}

fn syslog_send(stream: &mut TcpStream, bytes: &[u8]) -> Result<(), io::Error> {
    for byte in bytes {
        // we can write each byte on it's own, the TcpStream does buffer before sending (unless TCP_NODELAY has been set to true)
        stream.write_all(&[*byte])?;
        if *byte == b'\n' {
            syslog_prefix(stream)?;
        }
    }
    Ok(())
}

impl<const SIZE: usize> Default for LogBuffer<SIZE> {
    fn default() -> Self {
        Self {
            buffer: MaybeUninit::uninit_array(),
            last_sent: 0,
            write_position: 0,
            overflow: false,
            filled: false,
            // line: false,
            syslog_target: None,
            syslog_stream: None,
        }
    }
}

impl<const SIZE: usize> LogBuffer<SIZE> {
    const OVERFLOW_WARNING: &[u8] = b"WARNING: buffer overflow happened, some msgs got lost!\n";

    pub fn syslog_config(&mut self, host: &str, port: u16) {
        match (host, port).to_socket_addrs() {
            Ok(mut iter) => match iter.next() {
                Some(addr) => {
                    self.syslog_target.replace(addr);
                }
                None => error!("LogBuffer.syslog_config: to_socket_addrs returned None!"),
            },
            Err(err) => error!("LogBuffer.syslog_config: invalid target: {err}"),
        };
    }

    fn syslog_process(&mut self) -> Result<(), io::Error> {
        if self.syslog_stream.is_none() && let Some(target) = self.syslog_target {
            // try reconnecting, returning error if it fails -> the "?" operator
            debug!("LogBuffer.syslog connecting");
            let mut new_stream = TcpStream::connect_timeout(&target, Duration::from_millis(500))?;
            debug!("LogBuffer.syslog connected");
            // new_stream.set_nodelay(false)?; // Protocol not available (os error 109) // but it still looks like it does buffer and not send each bytes on it's own
            syslog_prefix(&mut new_stream)?;
            info!("LogBuffer.syslog connected & prefix sent");
            self.syslog_stream.replace(new_stream);
        };
        if let Some(stream) = self.syslog_stream.as_mut() {
            // try send
            // trace!("LogBuffer.syslog got stream, sending now");
            let ptr = self.buffer.as_mut_ptr() as *const u8;
            match self.last_sent.cmp(&self.write_position) {
                Less => syslog_send(
                    stream,
                    unsafe {
                        slice::from_raw_parts(
                            ptr.add(self.last_sent),
                            self.write_position - self.last_sent,
                        )
                    },
                    // unsafe { *ptr[self.last_sent..self.write_position] },
                    // &self.buffer.as_slice()[self.last_sent..self.write_position],
                )?,
                Greater => {
                    if self.overflow {
                        syslog_send(stream, Self::OVERFLOW_WARNING)?;
                    }
                    syslog_send(stream, &self.buffer[self.last_sent..])?;
                    syslog_send(stream, &self.buffer[..self.write_position])?;
                }
                Equal => (),
            };
            self.overflow = false;
            self.last_sent = self.write_position;
            Ok(())
        } else {
            // the only reason we could land here, is if syslog_target.is_none(),
            // because if only syslog_stream.is_none() we would have tried reconnecting
            // and if connecting failed, that error would have already been returned
            // by the "?" operator in the first if
            Err(io::Error::new(
                io::ErrorKind::InvalidInput,
                "no (valid) syslog target server specified",
            ))
        }
    }

    pub fn push_fast(&mut self, byte: u8) {
        if self.last_sent == (self.write_position + 1) {
            self.overflow = true;
            self.last_sent += 1;
        }
        // println!("log_buffer[{}]={}", self.write_position, byte as char);
        self.buffer[self.write_position] = byte;
        self.write_position = (self.write_position + 1) % SIZE;

        if !self.filled && self.write_position == SIZE - 1 {
            self.filled = true;
        }
        // if byte == b'\n' {
        //     self.line = true;
        // }
    }

    pub fn push(&mut self, byte: u8) {
        self.push_fast(byte);
        // if ((SIZE + self.write_position - self.last_sent) % SIZE) > 255 {
        if let Err(error) = self.syslog_process() {
            error!("LogBuffer.syslog_process failed: {error}");
        }
        // }
    }

    pub fn push_all(&mut self, bytes: &[u8]) {
        for &byte in &bytes[..=bytes.len() - 2] {
            self.push_fast(byte);
        }
        if let Some(&last_byte) = bytes.last() {
            self.push(last_byte);
        }
    }

    pub fn push_line(&mut self, line: &str) {
        self.push_all(line.as_bytes());
        self.push(b'\n');
    }

    pub fn read_all<'a>(&'a self) -> Box<dyn Iterator<Item = &'a u8> + 'a> {
        if self.filled {
            let part1 = &self.buffer[self.write_position..];
            let part2 = &self.buffer[..self.write_position];
            info!(
                "CustomBuffer.read_all - part1.len={}; part2.len={}",
                part1.len(),
                part2.len()
            );
            Box::new(part1.iter().chain(part2))
        } else {
            info!(
                "CustomBuffer.read_all not yet filled, last_written={}",
                self.write_position
            );
            Box::new(self.buffer[0..self.write_position].iter())
        }
    }

    // pub fn read_unread<'a>(&'a mut self) -> Box<dyn Iterator<Item = &'a u8> + 'a> {
    //     let result: Box<dyn Iterator<Item = &u8>> = if self.overflow {
    //         let part1 = &self.buffer[self.write_position..];
    //         let part2 = &self.buffer[..self.write_position];
    //         Box::new(Self::OVERFLOW_WARNING.iter().chain(part1).chain(part2))
    //     } else if self.write_position >= self.last_sent {
    //         Box::new(self.buffer[self.last_sent..self.write_position].iter())
    //     } else {
    //         let part1 = &self.buffer[self.last_sent..];
    //         let part2 = &self.buffer[..self.write_position];
    //         Box::new(part1.iter().chain(part2))
    //     };
    //     self.last_sent = self.write_position;
    //     // self.line = false;
    //     self.overflow = false;
    //     result
    // }
}

fn log_marker(level: Level) -> &'static str {
    match level {
        Level::Error => "E",
        Level::Warn => "W",
        Level::Info => "I",
        Level::Debug => "D",
        Level::Trace => "V",
    }
}

fn log_color(level: Level) -> Option<u8> {
    match level {
        Level::Error => Some(31), // LOG_COLOR_RED
        Level::Warn => Some(33),  // LOG_COLOR_BROWN
        Level::Info => Some(32),  // LOG_COLOR_GREEN,
        _ => None,
    }
}

pub struct BufferedLogger<const SIZE: usize> {
    buffer: Mutex<LogBuffer<SIZE>>,
}

impl<const SIZE: usize> Default for BufferedLogger<SIZE> {
    fn default() -> Self {
        Self {
            buffer: Mutex::new(LogBuffer::default()),
        }
    }
}

impl<const SIZE: usize> BufferedLogger<SIZE> {
    pub fn buffer_lock(&self) -> LockResult<MutexGuard<'_, LogBuffer<SIZE>>> {
        self.buffer.lock()
    }
}

fn delayed_log(msg: String) {
    warn!("BufferedLogger.buffer.try_lock failed, spawning a thread that does a blocking lock to write new msg to log once the buffer is accessible again.");
    std::thread::Builder::new()
        .name("delayed_log".into())
        .stack_size(4096) // 512 is too small, consistently causes stack overflow instantly. 2300 seems to work fine, 2000 causes stack overflow onces connection has been established, 2150 too but only sometimes. (2023-05-25 22:40)
        .spawn(move || match APP_STATE.buffered_logger.buffer_lock() {
            Ok(mut buffer) => buffer.push_line(&msg),
            Err(err) => error!("buffered_logger mutex poisoned! error={err}"),
        })
        .ok();
}

impl<const SIZE: usize> log::Log for BufferedLogger<SIZE> {
    fn enabled(&self, _metadata: &log::Metadata) -> bool {
        true
    }

    fn log(&self, record: &log::Record) {
        if self.enabled(record.metadata()) {
            let marker = log_marker(record.level());
            let module = record.module_path().unwrap_or("unknown");
            let file = record.file().unwrap_or("");
            let line = record.line().unwrap_or(0);
            let raw_msg = record.args();
            let msg = format!("{marker} [{module}] ({file}:{line}) {raw_msg}");
            match log_color(record.level()) {
                Some(color) => {
                    println!("\x1b[0;{color}m{msg}\x1b[0m");
                    // self.send(format!("\x1b[0;{color}m{remote_msg}\x1b[0m")).ok();
                }
                None => {
                    println!("{msg}");
                }
            };
            match self.buffer.try_lock() {
                Ok(mut buffer) => buffer.push_line(&msg),
                Err(_) => delayed_log(msg),
            }
        }
    }

    fn flush(&self) {}
}
